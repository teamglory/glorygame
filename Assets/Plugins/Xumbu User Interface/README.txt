If there is no 'Xumbu User Interface.dll' file inside '/Assets/Xumbu User Interface/Xumbu User Interface _main'
rename 'Xumbu User Interface.bytes' to 'Xumbu User Interface.dll'



Documentation:
http://www.xumbu.org/unity3d/user-interface/doc

Support:
https://www.facebook.com/Xumbu.User.Interface.Unity3D.GUI
dev@xumbu.org