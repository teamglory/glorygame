using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.Collections;
using Xumbu;


public class CustomButton : XButton
{
	protected override void InitAttributesLeftover(List<XmlAttribute> attributes)
	{
		base.InitAttributesLeftover(attributes);
		this.Label = this.Label.ToUpper();

		this.AddMouseListener(new StretchOnMouse(this, 1, 0.5f, 1.5f));
	}

	public override void Repaint ()
	{
		base.Repaint ();
	}
	public CustomButton(XuiManager gui, XContainer parent) : base(gui, parent)
	{
		this.RotationAngle=-10;
	}

	public override void ActionEvent(XActionEvent evt)
	{
		base.ActionEvent(evt);
		XComponentEffects.Rotate(this, 2);

	}
}


