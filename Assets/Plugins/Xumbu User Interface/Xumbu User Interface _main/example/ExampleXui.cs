using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System;
using System.Reflection;
using Xumbu;

public class ExampleXui : Xui_implementation 
{
	
	private string xmlPath = "Assets/Plugins/Xumbu User Interface" +
		"/Xumbu User Interface _main/example/exampleLayout.xml";
	private ArrayList itemList = new ArrayList();
	private ArrayList itemList2 = new ArrayList();
	
	void Start ()
	{
		for(int i = 0; i < 10; i++)
		{
			itemList2.Add(null); //add empty item slots
		}
		
		for(int i = 0; i < 3; i++)
		{
			itemList.AddRange(Item.createItems());
		}
		
		
		StartXuiManager(new XuiManager(this,xmlPath));
	}
	
	
	
	public override void InitXui(XuiManager manager)
	{
		/*find components from xml layout*/
		XComponent hungryBox = manager.findById("hungryBox");
		XDynamicContainer inventory  = (XDynamicContainer)manager.findById("inventoryDynContainer");
		XDynamicContainer inventoryQuickstart  = (XDynamicContainer)manager.findById("inventoryQuickstart");
		XComponent draggableContainer = manager.findById("draggableContainer");
		
		
		draggableContainer.AddMouseListener(new XComponentDrag(draggableContainer));
		
		InventoryDragAndDrop inventoryDragAndDrop = new InventoryDragAndDrop(inventory, this);
		inventory.AddMouseListener(inventoryDragAndDrop);
		
		inventoryDragAndDrop.AddDropListener(hungryBox, new HungryContainer(this, hungryBox));
		inventoryDragAndDrop.AddDropListener(inventoryQuickstart, new QuickStartDrop(inventoryQuickstart, inventory));
		
		
		
		
		hungryBox.AddMouseListener(new FadeOnMouseOver(hungryBox, 1, 1.5f));
		hungryBox.AddMouseListener(new FadeOnMouseOver(manager.findById("label"), 0.6f, 2));
		
	}
	
	
	
	public void OnAction_myButton(XActionEvent evt)
	{
		Debug.Log("button clicked");
	}
	public void OnAction_itemButton(XActionEvent evt)
	{
		Debug.Log(itemList[evt.Index].ToString() + " selected");
	}
	
	
	
	public ArrayList GetInventory
	{
		get
		{
			return itemList;
		}
	}
	
	public ArrayList GetQuickstartItems()
	{
		return itemList2;
	}
	
	
	
	public class QuickStartDrop : XDropListener
	{
		
		private XDynamicContainer quickStartContainer;
		private XDynamicContainer itemContainer;
		public QuickStartDrop(XDynamicContainer quickStartContainer, XDynamicContainer itemContainer)
		{
			this.quickStartContainer = quickStartContainer;
			this.itemContainer = itemContainer;
		}
		public void OnDrop (XDropEvent evt)
		{
			int targetIndex = quickStartContainer.GetIndexAtPoint(evt.DropPoint);
			
			if(targetIndex >= 0)
			{
				if(evt.DraggedObject != null)
				{
					quickStartContainer.GetTargetList()[targetIndex] = evt.DraggedObject;
				}

				itemContainer.Repaint();
				quickStartContainer.Repaint();
			}
		}
		
		public void OnDragOver (Vector2 dragginPosition)
		{
		}
	}
	
	
	
	public class HungryContainer : XDropListener
	{
		
		ExampleXui gui;
		private XComponent hungryComponent;
		public HungryContainer(ExampleXui gui, XComponent theHungryComponent)
		{
			this.gui = gui;
			hungryComponent = theHungryComponent;
		}
		
		public void OnDrop (XDropEvent evt)
		{
			gui.GetInventory[evt.Index] = null;
			XComponentEffects.Rotate(hungryComponent, 5);
			evt.DynamicContainer.Repaint();
		}
		
		public void OnDragOver (Vector2 dragginPosition)
		{
		}
		
	}
	
	
	
	public class InventoryDragAndDrop : XDragAndDrop
	{
		ExampleXui xui;
		public InventoryDragAndDrop(XDynamicContainer container, ExampleXui xui) : base(container)
		{
			this.xui = xui;
		}
		
		public override void OnOutVoidDrop (XDropEvent evt){}
		
		public override void OnSelfDrop (XDropEvent evt, int targetIndex)
		{
			int target = targetIndex >= 0 ? targetIndex : xui.GetInventory.Count-1;
			
			if(evt.Index >= 0 && target >= 0)
			{
				object tmp = xui.GetInventory[target];
				xui.GetInventory[target] = xui.GetInventory[evt.Index];
				xui.GetInventory[evt.Index] = tmp;
				DynamicContainer.Repaint();
			}
		}
		
		public override void OnOutDrop (XDropEvent evt)
		{}
		
		public override void OnDrag (object obj)
		{}
	}
}