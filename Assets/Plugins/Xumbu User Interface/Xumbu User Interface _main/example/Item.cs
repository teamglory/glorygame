using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item
{
	public static ArrayList createRandomItems()
	{
		ArrayList itemList = new ArrayList();
		for(int i = 0; i < 19; i++)
		{
			itemList.Add(new Item(""+i, UnityEngine.Random.Range(0, 100)));
		}
		
		return itemList;
	}
	
	public static ArrayList createItems()
	{
		ArrayList itemList = new ArrayList();
		
		HashSet<string> items = new HashSet<string>();
		
		UnityEngine.Object[] rpgImages = Resources.LoadAll("xui/Images/rpg");
		
		foreach(Texture2D o in rpgImages)
		{
			string itemName = o.name.Split('_')[0];
			if(!items.Contains(itemName))
			{
				items.Add(itemName);
				Item item = new Item(itemName, UnityEngine.Random.Range(1, 100));
				
				
				item.ImagePath = "xui/Images/rpg/"+o.name;
				itemList.Add(item);
			}
			
		}
		
		return itemList;
	}
	
	private string imagePath = "";
	private string itemname;
	private int amount = 0;
	
	public override string ToString ()
	{
		return string.Format ("[Item: Itemname={0}]", Itemname);
	}
	
	
	public Item(string name, int amount)
	{
		this.itemname = name;
		this.amount = amount;
		
	}
	public Item(string name)
	{
		this.itemname = name;
		
	}
	
	public int Amount {
		get {
			return this.amount;
		}
		set {
			amount = value;
		}
	}
	public string Itemname {
		get {
			return this.itemname;
		}
		set {
			itemname = value;
		}
	}
	
	public string ImagePath {
		get {
			return this.imagePath;
		}
		set {
			imagePath = value;
		}
	}
}