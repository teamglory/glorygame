﻿using UnityEngine;
using System.Collections;

public class LV1_Teleporter_NextLevel : MonoBehaviour {
	
	private void OnTriggerEnter(Collider col) {
		if(col.gameObject.tag == "Player") {
			Application.LoadLevel("FinalBossScene");
		}
	}
}
