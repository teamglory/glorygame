﻿using UnityEngine;

[RequireComponent(typeof(CharacterHealth))]
[RequireComponent(typeof(SimpleColor))]
public class TestTarget : MonoBehaviour {
	public GameObject prefabBloodSpurt = null;

	private void OnApplyDamage(DamageSource damage) {
		if(prefabBloodSpurt != null && damage.name == "Projectile") {
			GameObject obj = Instantiate(prefabBloodSpurt,damage.inflictionLocation,Quaternion.LookRotation(damage.inflictionNormal)) as GameObject;
			obj.transform.parent = SDynamicObjects.instance.transform;
			DelayedAction.EnqueAction(obj,2.0f,() => {
				Destroy(obj);
			});
		}
	}

	private void OnDie() {
		var health = GetComponent<CharacterHealth>();
		var color = GetComponent<SimpleColor>();
		health.healthCur = health.healthMax;
		color.objectColor.r = Random.value;
		color.objectColor.g = Random.value;
		color.objectColor.b = Random.value;
	}
}