using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Core;
using RAIN.Utility;
using RAIN.Action;
using RAIN.Navigation;


[RAINAction]
public class WanderField : RAINAction
{
    public WanderField()
    {
        actionName = "WanderField";
    }

    public override void Start(AI ai)
    {
        base.Start(ai);
    }

    public override ActionResult Execute(AI ai)
    {
		Vector3 loc = Vector3.zero;
		List<RAIN.Navigation.Graph.RAINNavigationGraph> found = new List<RAIN.Navigation.Graph.RAINNavigationGraph>();
		List<string> tags = new List<string>();
		
		do
		{
			loc = new Vector3(ai.Kinematic.Position.x + Random.Range(-8f, 8f), 
			                  ai.Kinematic.Position.y, 
			                  ai.Kinematic.Position.z + Random.Range(-8f, 8f));
			
			found = NavigationManager.Instance.GraphsForPoints(ai.Kinematic.Position, 
			                                                   loc, 
			                                                   ai.Motor.StepUpHeight, 
			                                                   NavigationManager.GraphType.Navmesh, 
			                                                   null);
			
			//(BasicNavigator)ai.Navigator).GraphTags
		}while ((Vector3.Distance(ai.Kinematic.Position, loc) < 2f) || (found.Count == 0 ));
		
		ai.WorkingMemory.SetItem<Vector3> ("varField", loc);
		return ActionResult.SUCCESS;
    }

    public override void Stop(AI ai)
    {
        base.Stop(ai);
    }
}