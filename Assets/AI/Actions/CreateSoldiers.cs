﻿using UnityEngine;
using System.Collections;
//PrefabEnemySoldier
public class CreateSoldiers : MonoBehaviour{
	public Transform target = null;
	public Transform target2 = null;
	public Transform target3 = null;
	public Transform target4 = null;
	public GameObject soldier = null;

	void OnTriggerEnter (Collider other)
	{
		Debug.Log("Object entered the Trigger");
		if(target != null && soldier != null) {
			GameObject newObj = (GameObject)Instantiate(soldier, target.position, target.rotation);
			newObj.transform.parent = SDynamicObjects.instance.transform;

			GameObject newObj2 = (GameObject)Instantiate(soldier, target2.position, target.rotation);
			newObj.transform.parent = SDynamicObjects.instance.transform;

			GameObject newObj3 = (GameObject)Instantiate(soldier, target3.position, target.rotation);
			newObj.transform.parent = SDynamicObjects.instance.transform;

			GameObject newObj4 = (GameObject)Instantiate(soldier, target4.position, target.rotation);
			newObj.transform.parent = SDynamicObjects.instance.transform;
		}
	}

	void OnTriggerStay (Collider other)
	{
		Debug.Log ("Object is within trigger");
	}

	void OnTriggerExit (Collider other)
	{
		Debug.Log("Object Exited the Trigger");
	}
}
