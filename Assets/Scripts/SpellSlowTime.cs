﻿using UnityEngine;

[RequireComponent(typeof(BaseMagic))]
public class SpellSlowTime : MonoBehaviour {
	[System.Serializable]
	public class Sound {
		public RandomSoundClip start;
		public RandomSoundClip stop;

		public RandomSoundClip holdLoop;
	}

	[HideInInspector]
	public BaseMagic magic;

	public BaseWeapon.FireStyle_SemiAuto styleSemiAuto;

	public Sound sound;

	public float manaInstantUsePerc = 0.3f;

	public float timeScaleMult = 0.8f;

	//public float transitionTime = 1.0f;

	private void Reset() {
		styleSemiAuto = new BaseWeapon.FireStyle_SemiAuto();
		styleSemiAuto.rateOfFire = 3.0f;
		styleSemiAuto.accuracy = 0.0f;
		styleSemiAuto.damageBase = 0.0f;
		styleSemiAuto.muzzleVelocity = 0.0f;
		styleSemiAuto.muzzleVelocityRand = 0.0f;
	}

	private void Awake() {
		magic = GetComponent<BaseMagic>();
	}

	private void Start() {
		magic.weapon.canFire = () => magic.weapon.equipped && magic.manaSource != null && magic.manaSource.manaCur >= magic.manaConsumption * manaInstantUsePerc;
		magic.weapon.fireStyleCur = styleSemiAuto;
	}

	private void OnWeaponFire() {
		// negate normal mana consumption, it is handled in a custom way to give a smooth "sustained" drain
		magic.manaSource.manaCur += magic.manaConsumption * (1.0f - manaInstantUsePerc);
	}

	private void OnWeaponSemiAutoFire() {
		SendMessage("OnEffectSlowTimeStart");
	}

	private void OnWeaponSemiAutoTriggerHeld() {
		magic.manaSource.manaCur -= magic.manaConsumption * Time.deltaTime;
	}

	private void OnWeaponSemiAutoTriggerRelease() {
		SendMessage("OnEffectSlowTimeStop");
	}

	private void OnEffectSlowTimeStart() {
		Time.timeScale *= timeScaleMult;

		if(audio != null) {
			AudioClip snd = sound.start.randomClip;
			if(snd != null) {
				audio.loop = false;
				audio.pitch = 1.0f;
				audio.PlayOneShot(snd);
			}
		}

		if(magic.weapon.handAnimatior != null) {
			magic.weapon.handAnimatior.SetTrigger("OnEffectBegin");
		}
	}

	private void OnEffectSlowTimeStop() {
		//Time.timeScale *= 1.0f / timeScaleMult;
		Time.timeScale = 1;

		if(audio != null) {
			AudioClip snd = sound.stop.randomClip;
			if(snd != null) {
				audio.loop = false;
				audio.pitch = 1.0f;
				audio.PlayOneShot(snd);
			}
		}

		if(magic.weapon.handAnimatior != null) {
			magic.weapon.handAnimatior.SetTrigger("OnEffectEnd");
		}
	}

	private void OnWeaponEquip(BaseWeapon weapon) {
		if(magic.weapon.handAnimatior != null) {
			magic.weapon.handAnimatior.ResetTrigger("OnEffectBegin");
			magic.weapon.handAnimatior.ResetTrigger("OnEffectEnd");
		}
	}

	private void OnWeaponUnequip() {
		if(magic.weapon.handAnimatior != null) {
			magic.weapon.handAnimatior.ResetTrigger("OnEffectBegin");
			magic.weapon.handAnimatior.ResetTrigger("OnEffectEnd");
		}
	}
}