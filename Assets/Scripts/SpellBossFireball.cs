﻿using UnityEngine;
using System.Collections;

public class SpellBossFireball : MonoBehaviour {
	[System.Serializable]
	public class Sound {
		public RandomSoundClip fire;
	}

	[HideInInspector]
	public BaseMagic magic;

	public BaseWeapon.FireStyle_FullyAuto styleAuto;

	public GameObject projectilePrefab;

	public Sound sound;

	private void Reset() {
		styleAuto = new BaseWeapon.FireStyle_Charge();

		styleAuto.rateOfFire = 1.0f;
		styleAuto.accuracy = 0.0f;
		styleAuto.damageBase = 50.0f;
		styleAuto.muzzleVelocity = 30.0f;
		styleAuto.muzzleVelocityRand = 2.0f;
	}

	private void Awake() {
		magic = GetComponent<BaseMagic>();
	}

	private void Start() {
		magic.weapon.canFire = () => magic.weapon.equipped && magic.manaSource != null && magic.manaSource.manaCur >= magic.manaConsumption;
		magic.weapon.fireStyleCur = styleAuto;
	}

	private void OnWeaponFire(BaseWeapon weapon) {
		if(audio != null) {
			if(audio.isPlaying) {
				audio.Stop();
			}
			audio.PlayOneShot(sound.fire.randomClip);
		}

		if(projectilePrefab != null) {
			GameObject newObj = (GameObject)Instantiate(projectilePrefab,magic.weapon.projectilePosition,Quaternion.identity);
			newObj.transform.parent = SDynamicObjects.instance.transform;

			var asProjectile = newObj.GetComponent<Projectile>();

			if(asProjectile != null) {
				asProjectile.owner = magic.weapon.wielder;
				asProjectile.kinimatics.velocity = magic.weapon.fireStyleCur.calculateFireDirection(magic.weapon.projectileDirection) * (magic.weapon.fireStyleCur.muzzleVelocity + Random.Range(-magic.weapon.fireStyleCur.muzzleVelocityRand,magic.weapon.fireStyleCur.muzzleVelocityRand));

				if(asProjectile.graphics.targetTrans != null) {
					asProjectile.graphics.targetTrans.position = magic.weapon.muzzleTrans.position;
				}
				asProjectile.damage.damageBase = magic.weapon.fireStyleCur.damageBase;
				asProjectile.damage.damageDstFalloff = magic.weapon.fireStyleCur.damageDstFalloff;
			}
		}
	}
}