﻿using UnityEngine;
using System.Collections;

public class SpellFireball : MonoBehaviour {
	[System.Serializable]
	public class Sound {
		public RandomSoundClip fire;

		public RandomSoundClip charging;

		public RandomSoundClip chargedLoop;

		[HideInInspector]
		public float chargePitchTmp;
	}

	[HideInInspector]
	public BaseMagic magic;

	public BaseWeapon.FireStyle_Charge styleCharge;

	public GameObject projectilePrefab;

	public Sound sound;

	private void Reset() {
		styleCharge = new BaseWeapon.FireStyle_Charge();

		styleCharge.chargeTime = 2.0f;
		styleCharge.chargeMinimum = 0.5f;

		styleCharge.rateOfFire = 1.0f;
		styleCharge.accuracy = 0.0f;
		styleCharge.damageBase = 50.0f;
		styleCharge.muzzleVelocity = 30.0f;
		styleCharge.muzzleVelocityRand = 2.0f;
	}

	private void Awake() {
		magic = GetComponent<BaseMagic>();
	}

	private void Start() {
		magic.weapon.canFire = () => magic.weapon.equipped && magic.manaSource != null && magic.manaSource.manaCur >= magic.manaConsumption;
		magic.weapon.fireStyleCur = styleCharge;
	}

	private void OnWeaponFire() {
		magic.manaSource.manaCur -= magic.manaConsumption;
	}

	private void OnWeaponChargeBegin(BaseWeapon weapon) {
		if(magic.weapon.handAnimatior != null) {
			magic.weapon.handAnimatior.SetTrigger("OnEffectBegin");
			magic.weapon.handAnimatior.speed = 1.0f / styleCharge.chargeTime;
		}

		if(audio != null) {
			sound.chargePitchTmp = audio.pitch;
			AudioClip snd = sound.charging.randomClip;
			if(snd != null) {
				audio.loop = false;
				audio.pitch = snd.length / styleCharge.chargeTime;
				audio.clip = snd;
				audio.Play();
			}
		}
	}

	private void OnWeaponCharging(BaseWeapon weapon) {
	}

	private void OnWeaponChargeStop(BaseWeapon weapon) {
		if(audio != null) {
			if(audio.isPlaying) {
				audio.Stop();
			}
		}

		if(magic.weapon.handAnimatior != null) {
			magic.weapon.handAnimatior.speed = 1.0f;
		}
	}

	private void OnWeaponChargeComplete(BaseWeapon weapon) {
		if(audio != null) {
			AudioClip snd = sound.chargedLoop.randomClip;
			if(snd != null) {
				audio.loop = true;
				audio.clip = snd;
				audio.Play();
			}
		}
	}

	private void OnWeaponChargeFireInsufficientCharge(BaseWeapon weapon) {
		if(magic.weapon.handAnimatior != null) {
			magic.weapon.handAnimatior.SetTrigger("OnEffectEnd");
		}
	}

	private void OnWeaponChargeFire(BaseWeapon weapon) {
		if(audio != null) {
			if(audio.isPlaying) {
				audio.Stop();
			}
			audio.pitch = sound.chargePitchTmp;
			audio.loop = false;
			audio.PlayOneShot(sound.fire.randomClip);
		}

		if(magic.weapon.handAnimatior != null) {
			magic.weapon.handAnimatior.SetTrigger("OnEffectEnd");
		}

		if(projectilePrefab != null) {
			GameObject newObj = (GameObject)Instantiate(projectilePrefab,magic.weapon.projectilePosition,Quaternion.identity);
			newObj.transform.parent = SDynamicObjects.instance.transform;

			var asProjectile = newObj.GetComponent<Projectile>();

			if(asProjectile != null) {
				asProjectile.owner = magic.weapon.wielder;
				asProjectile.kinimatics.velocity = magic.weapon.fireStyleCur.calculateFireDirection(magic.weapon.projectileDirection) * (magic.weapon.fireStyleCur.muzzleVelocity + Random.Range(-magic.weapon.fireStyleCur.muzzleVelocityRand,magic.weapon.fireStyleCur.muzzleVelocityRand));

				if(asProjectile.graphics.targetTrans != null) {
					asProjectile.graphics.targetTrans.position = magic.weapon.muzzleTrans.position;
				}
				asProjectile.damage.damageBase = magic.weapon.fireStyleCur.damageBase;
				asProjectile.damage.damageDstFalloff = magic.weapon.fireStyleCur.damageDstFalloff;
			}
		}
	}
}