﻿using UnityEngine;

[RequireComponent(typeof(BaseWeapon))]
public class BaseMagic : MonoBehaviour {

	[HideInInspector]
	public BaseWeapon weapon;

	public float manaConsumption = 10.0f;

	public Transform palmTrans = null;

	[HideInInspector]
	public CharacterMana manaSource { get; private set; }

	[HideInInspector]
	public Transform palmFX = null;

	//message callback
	private void OnWeaponFire() {
		manaSource.manaCur -= manaConsumption;
	}

	private void Awake() {
		weapon = GetComponent<BaseWeapon>();
		weapon.canFire = () => weapon.equipped && manaSource != null && manaSource.manaCur >= manaConsumption;

		palmFX = transform.FindChildRecursive("PalmFX");

		manaSource = null;
		aquireManaSource();
	}

	private void Update() {
		if(weapon.equipped && manaSource == null) {
			aquireManaSource();
		}
	}

	private void LateUpdate() {
		if(palmTrans != null && palmFX != null) {
			palmFX.position = palmTrans.position;
			palmFX.rotation = palmTrans.rotation;
		}
	}

	private void aquireManaSource() {
		if(weapon.wielder != null) {
			var source = weapon.wielder.transform.GetComponent<CharacterMana>();
			manaSource = source;
		}
	}
}