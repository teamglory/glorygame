﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]

[RequireComponent(typeof(Teleporter))]
public class EditorTeleporter : MonoBehaviour {
	private Teleporter teleporter = null;

	private void Start() {
		teleporter = GetComponent<Teleporter>();
	}

	void OnDrawGizmos() {
		if(teleporter != null) {
			const float _ALPHA = 0.75f;

			Gizmos.color = new Color(Color.yellow.r,Color.yellow.g,Color.yellow.b,_ALPHA);

			Gizmos.matrix = teleporter.transform.localToWorldMatrix;
			Gizmos.DrawWireCube(Vector3.zero,((BoxCollider)teleporter.collider).size);
			
			Gizmos.matrix = Matrix4x4.identity;

			if(teleporter.triggerTarget != null) {
				const float _ORILINELENGTH = 0.5f;

				Gizmos.DrawLine(transform.position,teleporter.triggerTarget.position);
				Gizmos.DrawSphere(teleporter.triggerTarget.position,0.5f);

				Gizmos.color = new Color(1,0,0,_ALPHA);
				Gizmos.DrawRay(teleporter.triggerTarget.position,teleporter.triggerTarget.right * _ORILINELENGTH);

				Gizmos.color = new Color(0,1,0,_ALPHA);
				Gizmos.DrawRay(teleporter.triggerTarget.position,teleporter.triggerTarget.up * _ORILINELENGTH);

				Gizmos.color = new Color(0,0,1,_ALPHA);
				Gizmos.DrawRay(teleporter.triggerTarget.position,teleporter.triggerTarget.forward * _ORILINELENGTH);

				Gizmos.color = new Color(0.5f,0.5f,0.5f,_ALPHA);
				Gizmos.DrawRay(teleporter.triggerTarget.position,-teleporter.triggerTarget.right * _ORILINELENGTH);
				Gizmos.DrawRay(teleporter.triggerTarget.position,-teleporter.triggerTarget.up * _ORILINELENGTH);
				Gizmos.DrawRay(teleporter.triggerTarget.position,-teleporter.triggerTarget.forward * _ORILINELENGTH);
			}
		}
	}
}
