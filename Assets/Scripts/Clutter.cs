﻿using UnityEngine;
using System.Collections;

public class Clutter : MonoBehaviour {

	public bool lifetimeEnabled = true;
	public float lifetimeSeconds = 10.0f;

	void Start () {
		if(lifetimeEnabled) DelayedAction.EnqueAction(gameObject,lifetimeSeconds,() => {
			Destroy(gameObject);
		});
	}
}
