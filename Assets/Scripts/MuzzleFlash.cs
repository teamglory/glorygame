﻿using UnityEngine;

public class MuzzleFlash : MonoBehaviour {
	public float lifeTime = 0.2f;
	public float lifeLights = 0.03f;

	private void Start() {
		DelayedMessage.EnqueMessage(gameObject,lifeTime,"OnKillSelf");
		DelayedMessage.EnqueMessage(gameObject,lifeLights,"OnKillLights");
	}

	private void OnKillSelf() {
		Destroy(gameObject);
	}

	private void OnKillLights() {
		var lightsTrans = transform.FindChild("Lights");
		if(lightsTrans != null) lightsTrans.gameObject.SetActive(false);
	}
}