﻿using UnityEngine;

/*
Messages:
OnAmmoChange(CharacterAmmo item);
*/

public class CharacterAmmo : MonoBehaviour {

	public enum AmmoType { RIFLE, SMG, PISTOL }

	public AmmoType ammoType = AmmoType.RIFLE;

	public int ammoMax = 100;
	public int ammoCur = -1;
	public float regenRate = 0.0f;

	public float regenMult = 1.0f;

	[HideInInspector]
	[SerializeField]
	private float regenCur = 0.0f;

	public float ammoPrev { get; private set; }

	private void Reset() {
		if(ammoCur == -1) ammoCur = ammoMax;
	}

	private void Start() {
		ammoPrev = ammoCur;
	}

	private void Update() {
		int regenFrame = 0;
		regenCur += regenRate * regenMult * Time.deltaTime;
		while(regenCur >= 1.0f) {
			regenFrame++;
			regenCur -= 1.0f;
		}
		ammoCur = Mathf.Min(ammoMax,ammoCur + regenFrame);
	}

	private void LateUpdate() {
		ammoCur = Mathf.Min(ammoMax,ammoCur);
		if(ammoCur != ammoPrev) {
			SendMessage("OnAmmoChange",this,SendMessageOptions.DontRequireReceiver);
		}
		ammoPrev = ammoCur;
	}
}