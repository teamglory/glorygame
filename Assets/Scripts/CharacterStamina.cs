﻿using UnityEngine;

/*
Messages:
OnStaminaChange(CharacterMana item);
*/

public class CharacterStamina : MonoBehaviour {
	public float staminaMax = 100.0f;
	public float staminaCur = -1.0f;
	public float regenRate = 10.0f;

	public float regenMult = 1.0f;

	public float staminaPrev { get; private set; }

	private void Reset() {
		if(staminaCur == -1.0f) staminaCur = staminaMax;
	}

	private void Start() {
		staminaPrev = staminaCur;
	}

	private void Update() {
		staminaCur = Mathf.Min(staminaMax,staminaCur + regenRate * regenMult * Time.deltaTime);
	}

	private void LateUpdate() {
		staminaCur = Mathf.Min(staminaMax,staminaCur);
		if(staminaCur != staminaPrev) {
			SendMessage("OnStaminaChange",this,SendMessageOptions.DontRequireReceiver);
		}
		staminaPrev = staminaCur;
	}
}