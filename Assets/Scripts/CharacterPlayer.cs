﻿using System;
using System.Collections.Generic;
using UnityEngine;

/*
Messages:
OnPlayerEquipGun(BaseGun weapon);
OnPlayerUnequipGun(BaseGun weapon);

OnPlayerEquipMagic(BaseMagic weapon);
OnPlayerUnequipMagic(BaseMagic weapon);
*/

[RequireComponent(typeof(CharacterMotor))]
[AddComponentMenu("Character/Character Input")]
public class CharacterPlayer : MonoBehaviour {
	public bool inputEnabled = true;

	[System.Serializable]
	public class CharacterCamera {
		public bool enabled = true;

		public bool invertX = false;
		public bool invertY = false;

		public Vector2 sensitivity = new Vector2(15.0f,15.0f);
		public float verticalRange = 100.0f;

		public float sprintSwayRotationPowerZ = 3.0f;
		public float sprintSwayRotationLerpPower = 7.0f;
		public float sprintSwayRotationFrequency = 0.75f;

		public bool freezeRigidBodyRotation = true;

		[System.NonSerialized]
		public float rotY = 0;

		[System.NonSerialized]
		public Transform camera = null;

		[System.NonSerialized]
		public Vector2 frameRotationDelta = Vector2.zero;
	}

	[System.Serializable]
	public class CharacterMovement {
		public bool enabled = true;
	}

	[System.Serializable]
	public class CharacterUI {

		[HideInInspector]
		public UISlider sliderHealth = null;

		[HideInInspector]
		public UISlider sliderMana = null;

		[HideInInspector]
		public UISlider sliderStamina = null;

		[HideInInspector]
		public UISprite spriteCrosshair = null;

		[HideInInspector]
		public UIAnchor spriteCrosshairAnchor = null;
	}

	[System.Serializable]
	public class CharacterWeapons {

		[System.Serializable]
		public class StoredWeapon {
			public Vector3 offsetPosCVD = Vector3.zero;
			public Vector3 offsetRot = Vector3.zero;

			public BaseWeapon weapon;
		}

		[System.Serializable]
		public class WeaponKinematics {

			[System.Serializable]
			public class LookSway {
				public Vector2 sensitivityAngular = new Vector2(3,3);
				public float recoveryAngular = 8.0f;
				public Vector2 sensitivityLinear = new Vector2(0.025f,0.025f);
				public float recoveryLinear = 10.0f;

				[System.NonSerialized]
				public Vector2 curOffsetAngular = Vector2.zero;

				[System.NonSerialized]
				public Vector2 curOffsetLinear = Vector2.zero;
			}

			[System.Serializable]
			public class MoveBob {

				[System.Serializable]
				public class MoveDirectionSettings {
					public float deadzone = 0.1f;

					[System.Serializable]
					public class Mode {
						public Vector3 sensitivity = new Vector3(8,8,8);
						public Vector3 phaseShift = Vector3.zero;
						public Vector3 strength = Vector3.zero;
						public float topSpeed = 15.0f;

						[System.NonSerialized]
						public Vector3 curTheta = Vector3.zero;

						[System.NonSerialized]
						public Vector3 curOffset = Vector3.zero;
					}

					public float onsetMult = 5.0f;
					public float recoveryMult = 10.0f;

					public Mode angular;
					public Mode linear;
				}

				public MoveDirectionSettings settingsForward;

				public MoveDirectionSettings settingsSideways;

				public Vector3 curOffsetAngular {
					get {
						return settingsForward.angular.curOffset + settingsSideways.angular.curOffset;
					}
				}

				public Vector3 curOffsetLinear {
					get {
						return settingsForward.linear.curOffset + settingsSideways.linear.curOffset;
					}
				}
			}

			public LookSway lookSway;

			public MoveBob moveBob;
		}

		public WeaponKinematics gunKinematics;
		public List<StoredWeapon> heldGuns;

		public StoredWeapon equippedGun {
			get {
				if(equippedGunId < 0 || equippedGunId >= heldGuns.Count)
					return null;
				else
					return heldGuns[equippedGunId];
			}
		}

		private int _equippedGunId = -1;

		public int equippedGunId {
			get {
				return _equippedGunId;
			}
			private set {
				_equippedGunId = value;
			}
		}

		public RandomSoundClip soundGunSwap;

		public WeaponKinematics magicKinematics;
		public List<StoredWeapon> heldMagic;

		public StoredWeapon equippedMagic {
			get {
				if(equippedMagicId < 0 || equippedMagicId >= heldMagic.Count)
					return null;
				else
					return heldMagic[equippedMagicId];
			}
		}

		private int _equippedMagicId = -1;

		public int equippedMagicId {
			get {
				return _equippedMagicId;
			}
			private set {
				_equippedMagicId = value;
			}
		}

		public RandomSoundClip soundMagicSwap;

		private CharacterPlayer _player = null;

		public CharacterPlayer player {
			get {
				return _player;
			}
			set {
				_player = value;
				foreach(var weapon in heldGuns) {
					weapon.weapon.wielder = _player.gameObject;
				}
				foreach(var weapon in heldMagic) {
					weapon.weapon.wielder = _player.gameObject;
				}
			}
		}

		/*
		 *  NOTE: Cannot disable unequipped weapons/magic, as there is a unity bug with animators being disabled.
		 *  The bug causes joints that are being effected by the currently running animation to be "frozen".
		 *  To work around this the script is just moving the unequipped items to the origin, where they will wait to be re-equipped.
		 * */

		public void EquipGun(int ind) {
			if(heldGuns.Count > 0) {
				ind = ind % heldGuns.Count;
				if(ind == equippedGunId) return;

				if(equippedGun != null) {
					equippedGun.weapon.equipped = false;
					equippedGun.weapon.gameObject.SetActive(false);
					player.SendMessage("OnPlayerUnequipGun",equippedGun.weapon.GetComponent<BaseGun>(),SendMessageOptions.DontRequireReceiver);
				}
				equippedGunId = ind;
				equippedGun.weapon.gameObject.SetActive(true);
				equippedGun.weapon.equipped = true;
				if(player != null) {
					equippedGun.weapon.wielder = player.gameObject;
					player.SendMessage("OnPlayerEquipGun",equippedGun.weapon.GetComponent<BaseGun>(),SendMessageOptions.DontRequireReceiver);
				}
			}
		}

		public void EquipMagic(int ind) {
			if(heldMagic.Count > 0) {
				ind = ind % heldMagic.Count;
				if(ind == equippedMagicId) return;

				if(equippedMagic != null) {
					equippedMagic.weapon.equipped = false;
					equippedMagic.weapon.gameObject.SetActive(false);
					player.SendMessage("OnPlayerUnequipMagic",equippedMagic.weapon.GetComponent<BaseMagic>(),SendMessageOptions.DontRequireReceiver);
				}
				equippedMagicId = ind;
				equippedMagic.weapon.gameObject.SetActive(true);
				equippedMagic.weapon.equipped = true;
				if(player != null) {
					equippedMagic.weapon.wielder = player.gameObject;
					player.SendMessage("OnPlayerEquipMagic",equippedMagic.weapon.GetComponent<BaseMagic>(),SendMessageOptions.DontRequireReceiver);
				}
			}
		}
	}

	[System.Serializable]
	public class ChararcterLife {
		public float spawnInvincibilityTime = 5.0f;

		public GameObject effectInvincibility = null;

		public float deathRespawnTime = 5.0f;

		[HideInInspector]
		public float timerInvincibility = 0.0f;

		[HideInInspector]
		public float timerDeathRespawn = -1.0f;
	}

	public CharacterCamera fpsCamera = new CharacterCamera();
	public CharacterMovement movement = new CharacterMovement();
	public CharacterUI ui = new CharacterUI();
	public CharacterWeapons weapons = new CharacterWeapons();
	public ChararcterLife life = new ChararcterLife();

	private CharacterMotor motor;

	private void Awake() {
		motor = GetComponent<CharacterMotor>();

		fpsCamera.camera = transform.FindChild("Camera");
		fpsCamera.rotY = -transform.localEulerAngles.x;

		if(GetComponent<Rigidbody>()) GetComponent<Rigidbody>().freezeRotation = true;

		weapons.player = this; // MUST BE SET BEFORE ANY EQUIP FUNCTION IS CALLED

		//de-prefab the guns passed in from the inspector
		for(int i = 0; i < weapons.heldGuns.Count; i++) {
			weapons.heldGuns[i].weapon = (BaseWeapon)Instantiate(weapons.heldGuns[i].weapon);
			weapons.heldGuns[i].weapon.transform.parent = SDynamicObjects.instance.transform;
			weapons.heldGuns[i].weapon.gameObject.SetActive(false);
		}
		if(weapons.heldGuns.Count > 0) weapons.EquipGun(0);

		for(int i = 0; i < weapons.heldMagic.Count; i++) {
			weapons.heldMagic[i].weapon = (BaseWeapon)Instantiate(weapons.heldMagic[i].weapon);
			weapons.heldMagic[i].weapon.transform.parent = SDynamicObjects.instance.transform;
			weapons.heldMagic[i].weapon.gameObject.SetActive(false);
		}
		if(weapons.heldMagic.Count > 0) weapons.EquipMagic(0);

		GameObject tmp;
		tmp = GameObject.Find("slider_health");
		if(tmp != null) ui.sliderHealth = tmp.GetComponent<UISlider>();

		tmp = GameObject.Find("slider_mana");
		if(tmp != null) ui.sliderMana = tmp.GetComponent<UISlider>();

		tmp = GameObject.Find("slider_stamina");
		if(tmp != null) ui.sliderStamina = tmp.GetComponent<UISlider>();

		tmp = GameObject.Find("crosshair");
		if(tmp != null) ui.spriteCrosshair = tmp.GetComponent<UISprite>();
		if(ui.spriteCrosshair) {
			ui.spriteCrosshairAnchor = ui.spriteCrosshair.GetComponent<UIAnchor>();
		}
	}

	private void Start() {
		SendMessage("OnSpawn");
	}

	private void Update() {
		if(!Screen.lockCursor && Input.GetMouseButtonUp(0)) {
			Screen.lockCursor = true;
		}
		if(Input.GetButtonUp("Menu")) {
			Screen.lockCursor = !Screen.lockCursor;
		}

		if(weapons.player != this) weapons.player = this;

		if(inputEnabled) {
			Vector2 mouse = !Screen.lockCursor ? Vector2.zero : new Vector2(Input.GetAxis("Mouse X"),Input.GetAxis("Mouse Y"));
			Vector3 direction = !Screen.lockCursor ? Vector3.zero : new Vector3(Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical"));

			if(fpsCamera.enabled && fpsCamera.camera != null && Screen.lockCursor) {
				fpsCamera.frameRotationDelta.x = mouse.x * fpsCamera.sensitivity.x * (fpsCamera.invertX ? -1 : 1);
				float rotX = transform.localEulerAngles.y + fpsCamera.frameRotationDelta.x;
				fpsCamera.frameRotationDelta.y = mouse.y * fpsCamera.sensitivity.y * (fpsCamera.invertY ? -1 : 1);
				if(fpsCamera.rotY + fpsCamera.frameRotationDelta.y > fpsCamera.verticalRange) {
					fpsCamera.frameRotationDelta.y = fpsCamera.verticalRange - fpsCamera.rotY;
				} else if(fpsCamera.rotY + fpsCamera.frameRotationDelta.y < -fpsCamera.verticalRange) {
					fpsCamera.frameRotationDelta.y = -fpsCamera.verticalRange - fpsCamera.rotY;
				}
				fpsCamera.rotY += fpsCamera.frameRotationDelta.y;

				transform.localEulerAngles = new Vector3(0,rotX,0);
				fpsCamera.camera.localEulerAngles = new Vector3(-fpsCamera.rotY,0,0);
			}

			if(movement.enabled) {
				if(direction != Vector3.zero) {
					float directionLen = direction.magnitude;
					direction /= directionLen;

					directionLen = Mathf.Min(1,directionLen);

					// increase sensitivity to the extremes and reduce it to the minimums, for controller users
					directionLen *= directionLen;

					direction *= directionLen;
				}

				motor.input.moveDirection = transform.rotation * direction;
				motor.input.jump = Screen.lockCursor && Input.GetButton("Jump");
			}

			Action<CharacterWeapons.WeaponKinematics> lam_runKinimatics = (CharacterWeapons.WeaponKinematics kinimaticType) => {
				//apply kinimatics (sway)
				for(int i = 0; i < 2; i++) {
					kinimaticType.lookSway.curOffsetAngular[i] += Time.deltaTime * kinimaticType.lookSway.sensitivityAngular[i] * fpsCamera.frameRotationDelta[i];
					kinimaticType.lookSway.curOffsetAngular[i] = Mathf.Lerp(kinimaticType.lookSway.curOffsetAngular[i],0,kinimaticType.lookSway.recoveryAngular * Time.deltaTime);

					kinimaticType.lookSway.curOffsetLinear[i] += Time.deltaTime * kinimaticType.lookSway.sensitivityLinear[i] * fpsCamera.frameRotationDelta[i];
					kinimaticType.lookSway.curOffsetLinear[i] = Mathf.Lerp(kinimaticType.lookSway.curOffsetLinear[i],0,kinimaticType.lookSway.recoveryLinear * Time.deltaTime);
				}

				//apply kinimatics (bob)
				Action<CharacterWeapons.WeaponKinematics.MoveBob.MoveDirectionSettings,float> lam_runBobLogic = (CharacterWeapons.WeaponKinematics.MoveBob.MoveDirectionSettings settings,float axis) => {
					Action<CharacterWeapons.WeaponKinematics.MoveBob.MoveDirectionSettings.Mode> lam_runModeLogic = (CharacterWeapons.WeaponKinematics.MoveBob.MoveDirectionSettings.Mode mode) => {
						for(int i = 0; i < 3; i++) {
							if(Mathf.Abs(axis) >= settings.deadzone) {
								mode.curTheta[i] += mode.sensitivity[i] * Mathf.Clamp01(axis/mode.topSpeed) * Time.deltaTime;
								while(mode.curTheta[i] > Mathf.PI * 2) mode.curTheta[i] -= Mathf.PI * 2;
								while(mode.curTheta[i] < -Mathf.PI * 2) mode.curTheta[i] += Mathf.PI * 2;

								float target = Mathf.Sin(mode.curTheta[i] + mode.phaseShift[i]) * mode.strength[i];
								mode.curOffset[i] = Mathf.Lerp(mode.curOffset[i],target,settings.onsetMult * Time.deltaTime);
							} else {
								mode.curOffset[i] = Mathf.Lerp(mode.curOffset[i],0,settings.recoveryMult * Time.deltaTime);
							}
						}
					};

					lam_runModeLogic(settings.angular);
					lam_runModeLogic(settings.linear);
				};

				lam_runBobLogic(kinimaticType.moveBob.settingsSideways,direction.x * Vector3.Dot(motor.controller.velocity,transform.right));
				lam_runBobLogic(kinimaticType.moveBob.settingsForward,direction.z * Vector3.Dot(motor.controller.velocity,transform.forward));
			};

			//camera sprint effects
			if(motor.input.sprint) {
				Vector3 newEualr = fpsCamera.camera.localEulerAngles;
				newEualr.z = HTUtil.PingPong(Time.time,fpsCamera.sprintSwayRotationFrequency,-fpsCamera.sprintSwayRotationPowerZ,fpsCamera.sprintSwayRotationPowerZ);
				fpsCamera.camera.localEulerAngles = Vector3.Lerp(fpsCamera.camera.localEulerAngles,newEualr,fpsCamera.sprintSwayRotationLerpPower * Time.deltaTime);
			} else {
				Vector3 newEualr = fpsCamera.camera.localEulerAngles;
				newEualr.z = 0.0f;
				fpsCamera.camera.localEulerAngles = Vector3.Lerp(fpsCamera.camera.localEulerAngles,newEualr,fpsCamera.sprintSwayRotationLerpPower * Time.deltaTime * 0.25f);
			}
			
			if(weapons.equippedMagic != null) {
				lam_runKinimatics(weapons.magicKinematics);

				weapons.equippedMagic.weapon.triggerDown = Screen.lockCursor && Input.GetButton("Fire2");
			}

			if(weapons.equippedGun != null) {
				lam_runKinimatics(weapons.gunKinematics);

				//firing/reloading
				weapons.equippedGun.weapon.triggerDown = Screen.lockCursor && Input.GetButton("Fire1");
				if(Screen.lockCursor && Input.GetButtonDown("Reload")) {
					BaseGun asGun = weapons.equippedGun.weapon.GetComponent<BaseGun>();
					if(!asGun.reloading) {
						asGun.reloadBegin();
					}
				}
			}

			if(Screen.lockCursor) {
				if(Input.GetButtonDown("SwapWeapon")) {
					weapons.EquipGun(weapons.equippedGunId + 1);
				}

				if(Input.GetButtonDown("SwapMagic")) {
					weapons.EquipMagic(weapons.equippedMagicId + 1);
				}

				motor.input.sprint = Input.GetButton("Sprint");
			}
		} else { // inputenabled == false
			motor.input.jump = false;
			motor.input.moveDirection = Vector3.zero;
			motor.input.sprint = false;
		}

		if(life.timerInvincibility > 0) {
			life.timerInvincibility -= Time.deltaTime;
			if(life.timerInvincibility <= 0) {
				if(life.effectInvincibility != null) {
					life.effectInvincibility.SetActive(false);
				}
			}
		}

		if(life.timerDeathRespawn > 0) {
			life.timerDeathRespawn -= Time.deltaTime;
			if(life.timerDeathRespawn <= 0) {
				SendMessage("OnSpawn");
			}
		}
	}

	private void LateUpdate() {
		Action<CharacterWeapons.WeaponKinematics,CharacterWeapons.StoredWeapon> lam_syncWeaponTransform = (CharacterWeapons.WeaponKinematics kinimaticType,CharacterWeapons.StoredWeapon storedWeapon) => {
			storedWeapon.weapon.projectileDirection = fpsCamera.camera.forward;
			storedWeapon.weapon.projectilePosition = fpsCamera.camera.position + storedWeapon.weapon.projectileDirection * 0.5f;

			Vector3 swayTranslation = new Vector3(kinimaticType.lookSway.curOffsetLinear.x,kinimaticType.lookSway.curOffsetLinear.y,0);
			swayTranslation += kinimaticType.moveBob.curOffsetLinear;
			storedWeapon.weapon.transform.position = fpsCamera.camera.gameObject.GetComponent<Camera>().ViewportToWorldPoint(storedWeapon.offsetPosCVD);
			storedWeapon.weapon.transform.position += fpsCamera.camera.rotation * swayTranslation;

			storedWeapon.weapon.transform.rotation = fpsCamera.camera.rotation;

			Vector3 swayRotation = new Vector3(-kinimaticType.lookSway.curOffsetAngular.y,kinimaticType.lookSway.curOffsetAngular.x,0);
			swayRotation += kinimaticType.moveBob.curOffsetAngular;
			storedWeapon.weapon.transform.Rotate(storedWeapon.offsetRot + swayRotation);
		};

		if(fpsCamera.enabled && fpsCamera.camera != null) {
			if(weapons.equippedGun != null) {
				lam_syncWeaponTransform(weapons.gunKinematics,weapons.equippedGun);
			}

			if(weapons.equippedMagic != null) {
				lam_syncWeaponTransform(weapons.magicKinematics,weapons.equippedMagic);
			}

			//place the crosshair according to where the bullets are coming from
			//disabled, as the bullets come from the camera
			// 			if(ui.spriteCrosshairAnchor != null) {
			// 				const float _CROSSHAIR_DISTANCE = 5.0f;
			// 				Vector3 newCrossHairCVD = fpsCamera.camera.GetComponent<Camera>().WorldToViewportPoint(transform.TransformPoint(weapons.projectileOffset) + fpsCamera.camera.forward * _CROSSHAIR_DISTANCE);
			// 				ui.spriteCrosshairAnchor.relativeOffset.x = newCrossHairCVD.x;
			// 				ui.spriteCrosshairAnchor.relativeOffset.y = newCrossHairCVD.y;
			// 			}
		}
	}

	private void OnHealthChange(CharacterHealth health) {
		if(ui.sliderHealth != null) ui.sliderHealth.sliderValue = Mathf.Clamp01(health.healthCur / health.healthMax);
	}

	private void OnManaChange(CharacterMana mana) {
		if(ui.sliderMana != null) ui.sliderMana.sliderValue = Mathf.Clamp01(mana.manaCur / mana.manaMax);
	}

	private void OnStaminaChange(CharacterStamina stamina) {
		if(ui.sliderStamina != null) ui.sliderStamina.sliderValue = Mathf.Clamp01(stamina.staminaCur / stamina.staminaMax);
	}

	private void OnApplyDamage(DamageSource damage) {
		if(life.timerInvincibility > 0) {
			if(damage.strength > 0) damage.strength = 0;
		}
	}

	private void OnDie() {
		life.timerDeathRespawn = life.deathRespawnTime;
		inputEnabled = false;

		if(weapons.equippedGun != null) {
			weapons.equippedGun.weapon.gameObject.SetActive(false);
		}

		if(weapons.equippedMagic != null) {
			weapons.equippedMagic.weapon.gameObject.SetActive(false);
		}

	}

	private void OnSpawn() {
		life.timerDeathRespawn = -1;
		inputEnabled = true;
		CharacterHealth health = GetComponent<CharacterHealth>();
		health.healthCur = health.healthMax;
		life.timerInvincibility = life.spawnInvincibilityTime;

		if(life.effectInvincibility != null) {
			life.effectInvincibility.SetActive(true);
		}

		if(weapons.equippedGun != null) {
			weapons.equippedGun.weapon.gameObject.SetActive(true);
		}

		if(weapons.equippedMagic != null) {
			weapons.equippedMagic.weapon.gameObject.SetActive(true);
		}
	}

	private void OnPlayerEquipGun(BaseGun gun) {
		var pogfx = gun.transform.FindChildRecursive("PlayerOnly");
		if(pogfx != null) {
			pogfx.gameObject.SetActive(true);
		}

		GameObject soundPlayer = HTUtil.SoundPlayAtLocationEx(gun.weapon.transform.position,weapons.soundGunSwap.randomClip);
		soundPlayer.transform.parent = gun.weapon.transform;
		soundPlayer.transform.localPosition = Vector3.zero;
	}

	private void OnPlayerUnequipGun(BaseGun gun) {
		var pogfx = gun.transform.FindChildRecursive("PlayerOnly");
		if(pogfx != null) {
			pogfx.gameObject.SetActive(false);
		}
	}

	private void OnPlayerEquipMagic(BaseMagic magic) {
		GameObject soundPlayer = HTUtil.SoundPlayAtLocationEx(magic.weapon.transform.position,weapons.soundMagicSwap.randomClip);
		soundPlayer.transform.parent = magic.weapon.transform;
		soundPlayer.transform.localPosition = Vector3.zero;
	}
}