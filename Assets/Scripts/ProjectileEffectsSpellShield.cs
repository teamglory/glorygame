﻿using UnityEngine;

[RequireComponent(typeof(Projectile))]
public class ProjectileEffectsSpellShield : MonoBehaviour {
	public float dissipateTime = 3.0f;

	public float dissipateTossSpeed = 15.0f;

	[HideInInspector]
	[SerializeField]
	private ParticleSystem[] psystems = new ParticleSystem[2];

	private void Awake() {
		for(int i = 0; i < psystems.Length; i++) psystems[i] = transform.Find("Graphics/Particles/system" + i).GetComponent<ParticleSystem>();
	}

	private void OnShieldDissipateUpdatePS(int ps,float newEmissionRate) {
		if(psystems[ps] != null) psystems[ps].emissionRate = newEmissionRate;
	}

	private void OnShieldDissipateUpdatePS0(float newEmissionRate) {
		OnShieldDissipateUpdatePS(0,newEmissionRate);
	}

	private void OnShieldDissipateUpdatePS1(float newEmissionRate) {
		OnShieldDissipateUpdatePS(1,newEmissionRate);
	}

	private void OnShieldDissipateBegin() {
		transform.parent = SDynamicObjects.instance.transform;

		rigidbody.useGravity = true;
		rigidbody.constraints = RigidbodyConstraints.None;
		rigidbody.velocity = transform.forward * dissipateTossSpeed;

		HTUtil.ObjectHierarchyBredthFirst(gameObject,(GameObject obj) => {
			obj.layer = LayerMask.NameToLayer("Projectile");
			if(obj.collider != null) {
				obj.transform.localScale = new Vector3(1,obj.transform.localScale.y,1);
			}
			return HTUtil.AlgorithmAction.CONTINUE;
		});

		for(int i = 0; i < psystems.Length; i++) {
			if(psystems[i] != null) {
				iTween.ValueTo(gameObject,iTween.Hash("time",dissipateTime,"from",psystems[i].emissionRate,"to",0,"onupdate","OnShieldDissipateUpdatePS" + i));
			}
		}

		DelayedMessage.EnqueMessage(gameObject,dissipateTime,"OnShieldDissipateFinish");
	}

	private void OnShieldDissipateFinish() {
		Destroy(gameObject);
	}
}