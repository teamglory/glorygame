﻿using UnityEngine;

[RequireComponent(typeof(BaseMagic))]
public class SpellShield : MonoBehaviour {
	[System.Serializable]
	public class Sound {
		public RandomSoundClip fire;

		public RandomSoundClip holdLoop;
	}

	[HideInInspector]
	public BaseMagic magic;

	[HideInInspector]
	public BaseWeapon.FireStyle_SemiAuto styleSemiAuto;

	public float manaInstantUsePerc = 0.3f;

	public GameObject shieldPrefab;

	public Vector3 shieldOffset = Vector3.zero;

	[HideInInspector]
	[SerializeField]
	private ProjectileEffectsSpellShield shieldCur = null;

	private void Reset() {
		styleSemiAuto = new BaseWeapon.FireStyle_SemiAuto();
		styleSemiAuto.rateOfFire = 1.0f;
		styleSemiAuto.accuracy = 0.0f;
		styleSemiAuto.damageBase = 0.0f;
		styleSemiAuto.muzzleVelocity = 0.0f;
		styleSemiAuto.muzzleVelocityRand = 0.0f;
	}

	private void Awake() {
		magic = GetComponent<BaseMagic>();
	}

	private void Start() {
		magic.weapon.canFire = () => magic.weapon.equipped && magic.manaSource != null && magic.manaSource.manaCur >= magic.manaConsumption * manaInstantUsePerc;
		magic.weapon.fireStyleCur = styleSemiAuto;
	}

	private void OnWeaponFire() {
		// negate normal mana consumption, it is handled in a custom way to give a smooth "sustained" drain
		magic.manaSource.manaCur += magic.manaConsumption * (1.0f - manaInstantUsePerc);
	}

	private void OnWeaponSemiAutoFire() {
		if(shieldCur != null) shieldCur.SendMessage("OnShieldDissipateBegin");

		shieldCur = ((GameObject)Instantiate(shieldPrefab,Vector3.zero,transform.rotation)).GetComponent<ProjectileEffectsSpellShield>();
		shieldCur.transform.parent = gameObject.transform;

		shieldCur.GetComponent<Projectile>().owner = magic.weapon.wielder;

		shieldCur.transform.localPosition = magic.weapon.muzzleTrans.localPosition + shieldOffset;

		if(magic.weapon.handAnimatior != null) {
			magic.weapon.handAnimatior.SetTrigger("OnEffectBegin");
		}
	}

	private void OnWeaponSemiAutoTriggerHeld() {
		magic.manaSource.manaCur -= magic.manaConsumption * Time.deltaTime;
	}

	private void OnWeaponSemiAutoTriggerRelease() {
		if(shieldCur != null) shieldCur.SendMessage("OnShieldDissipateBegin");
		shieldCur = null;

		if(magic.weapon.handAnimatior != null) {
			magic.weapon.handAnimatior.SetTrigger("OnEffectEnd");
		}
	}
}