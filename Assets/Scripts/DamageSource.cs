﻿using UnityEngine;

public class DamageSource {
	public string name;
	public float strength;
	public GameObject inflictor;

	public CharacterHealth target;

	public Vector3 inflictionLocation;
	public Vector3 inflictionNormal;

	public DamageSource(string name,float strength,GameObject inflictor,Vector3 inflictionLocation,Vector3 inflictionNormal) {
		this.name = name;
		this.strength = strength;
		this.inflictor = inflictor;
		this.inflictionLocation = inflictionLocation;
		this.inflictionNormal = inflictionNormal;
	}
}