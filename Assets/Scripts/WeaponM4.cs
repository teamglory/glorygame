﻿using UnityEngine;

[RequireComponent(typeof(BaseGun))]
public class WeaponM4 : MonoBehaviour {
	[System.Serializable]
	public class Sound {
		public RandomSoundClip fire;

		public RandomSoundClip reload;
		[HideInInspector]
		public float reloadPitchTmp;
	}

	[HideInInspector]
	public BaseGun gun;

	public BaseWeapon.FireStyle_FullyAuto styleFullyAuto;
	public GameObject projectilePrefab;

	public GameObject reloadClutterPrefab;

	public Sound sound;

	[HideInInspector]
	[SerializeField]
	private Transform magazineTrans;

	private void Reset() {
		styleFullyAuto = new BaseWeapon.FireStyle_FullyAuto();
		styleFullyAuto.rateOfFire = 11.6f; //700 rpm
		styleFullyAuto.accuracy = 0.05f;
	}

	private void Awake() {
		gun = GetComponent<BaseGun>();
		magazineTrans = transform.FindChildRecursive("Magazine");
	}

	private void Start() {
		gun.weapon.fireStyleCur = styleFullyAuto;
	}

	private void OnWeaponFire() {
		if(audio != null) {
			audio.PlayOneShot(sound.fire.randomClip);
		}

		if(projectilePrefab != null) {
			GameObject newObj = (GameObject)Instantiate(projectilePrefab,gun.weapon.projectilePosition,Quaternion.identity);
			newObj.transform.parent = SDynamicObjects.instance.transform;

			var asProjectile = newObj.GetComponent<Projectile>();

			if(asProjectile != null) {
				asProjectile.owner = gun.weapon.wielder;
				asProjectile.kinimatics.velocity = gun.weapon.fireStyleCur.calculateFireDirection(gun.weapon.projectileDirection) * (gun.weapon.fireStyleCur.muzzleVelocity + Random.Range(-gun.weapon.fireStyleCur.muzzleVelocityRand,gun.weapon.fireStyleCur.muzzleVelocityRand));

				if(asProjectile.graphics.targetTrans != null) {
					asProjectile.graphics.targetTrans.position = gun.weapon.muzzleTrans.position;
				}
				asProjectile.damage.damageBase = gun.weapon.fireStyleCur.damageBase;
				asProjectile.damage.damageDstFalloff = gun.weapon.fireStyleCur.damageDstFalloff;
			}
		}
	}

	private void OnReloadBegin() {
		if(audio != null) {
			sound.reloadPitchTmp = audio.pitch;
			AudioClip sndReload = sound.reload.randomClip;
			if(sndReload != null) {
				audio.pitch = sndReload.length / gun.reloadTime;
				audio.PlayOneShot(sndReload);
			}
		}

		if(magazineTrans && reloadClutterPrefab != null) {
			GameObject clutter = (GameObject)Instantiate(reloadClutterPrefab,magazineTrans.position,magazineTrans.rotation);
			clutter.transform.parent = SDynamicObjects.instance.transform;

			if(gun.weapon.wielder != null) {
				//find the wielder's rigidbody
				Rigidbody body = null;

				HTUtil.ObjectHierarchyBredthFirst(gun.weapon.wielder,(GameObject obj) => {
					if(obj.rigidbody != null) {
						body = obj.rigidbody;
						return HTUtil.AlgorithmAction.BREAK;
					}
					return HTUtil.AlgorithmAction.CONTINUE;
				});

				if(body) {
					clutter.rigidbody.velocity = body.velocity;
				} else {
					//failed, so try to find a character controller
					CharacterController controller = null;

					HTUtil.ObjectHierarchyBredthFirst(gun.weapon.wielder,(GameObject obj) => {
						var found = obj.GetComponent<CharacterController>();
						if(found != null) {
							controller = found;
							return HTUtil.AlgorithmAction.BREAK;
						}
						return HTUtil.AlgorithmAction.CONTINUE;
					});

					if(controller) {
						clutter.rigidbody.velocity = controller.velocity;
					}
				}
			}

			clutter.rigidbody.velocity += Vector3.down * 3.0f;

			magazineTrans.gameObject.SetActive(false);
		}
	}

	private void OnReloadCancel() {
		if(audio != null) {
			audio.pitch = sound.reloadPitchTmp;
		}
		if(magazineTrans) {
			magazineTrans.gameObject.SetActive(true);
		}
	}

	private void OnReloadComplete() {
		if(audio != null) {
			audio.pitch = sound.reloadPitchTmp;
		}
		if(magazineTrans) {
			magazineTrans.gameObject.SetActive(true);
		}
	}
}