﻿using UnityEngine;

[RequireComponent(typeof(Projectile))]
public class ProjectileEffectsNormal : MonoBehaviour {
	[System.Serializable]
	public class Sound {
		public RandomSoundClip impact;
	}

	public Sound sound;

	public GameObject debrisPrefab = null;

	private void OnProjectileCollision(RaycastHit hit) {
		HTUtil.SoundPlayAtLocationEx(hit.point,audio,sound.impact.randomClip);

		if(debrisPrefab != null && hit.transform.gameObject.isStatic) {
			Vector3 debrisForward = hit.normal;
			Vector3 debrisPos = hit.point + debrisForward*0.05f;
			Quaternion debrisRot = Quaternion.LookRotation(debrisForward);
			GameObject debris = (GameObject)Instantiate(debrisPrefab,debrisPos,debrisRot);
			debris.transform.parent = SDynamicObjects.instance.transform;
		}

		CharacterHealth otherHealth = hit.transform.GetComponent<CharacterHealth>();
		if(otherHealth != null) {
			var projectile = GetComponent<Projectile>();
			float damage = projectile.damage.damageBase * projectile.damage.damageDstFalloff.Evaluate(projectile.kinimatics.distanceTraveled);
			otherHealth.applyDamage(new DamageSource("Projectile",damage,gameObject,hit.point,hit.normal));
		}
	}
}