﻿using UnityEngine;
using System;

public class DelayedAction : MonoBehaviour {
	public float timeLeft;

	private Action action = null;

	private void Update() {
		timeLeft -= Time.deltaTime;
		if(timeLeft <= 0.0f) {
			if(action != null) action();
			Destroy(this);
		}
	}

	public static void EnqueAction(GameObject methodTarget,float delay,Action action) {
		var newAction = methodTarget.AddComponent<DelayedAction>();
		newAction.timeLeft = delay;
		newAction.action = action;
	}
}