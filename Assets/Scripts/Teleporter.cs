﻿using UnityEngine;
using System.Collections;

public class Teleporter : MonoBehaviour {
	public Transform triggerTarget = null;

	public LayerMask triggerMask;

	public float rotationChange = 1.0f;
	

	private void Reset() {
		triggerTarget = transform.parent.FindChild("Target");
	}

	private void OnTriggerEnter(Collider col) {
		if(triggerTarget == null) return;

		if((triggerMask.value & 1 << col.gameObject.layer) != 0) {
			col.gameObject.transform.position = triggerTarget.position;
			col.gameObject.transform.rotation = Quaternion.Slerp(col.gameObject.transform.rotation,triggerTarget.rotation,rotationChange);
		}
	}
}
