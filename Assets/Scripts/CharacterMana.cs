﻿using UnityEngine;

/*
Messages:
OnManaChange(CharacterMana item);
*/

public class CharacterMana : MonoBehaviour {
	public float manaMax = 100.0f;
	public float manaCur = -1.0f;
	public float regenRate = 10.0f;

	public float regenMult = 1.0f;

	public float manaPrev { get; private set; }

	private void Reset() {
		if(manaCur == -1.0f) manaCur = manaMax;
	}

	private void Start() {
		manaPrev = manaCur;
	}

	private void Update() {
		manaCur = Mathf.Min(manaMax,manaCur + regenRate * regenMult * Time.deltaTime);
	}

	private void LateUpdate() {
		manaCur = Mathf.Min(manaMax,manaCur);
		if(manaCur != manaPrev) {
			SendMessage("OnManaChange",this,SendMessageOptions.DontRequireReceiver);
		}
		manaPrev = manaCur;
	}
}