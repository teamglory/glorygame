﻿using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[AddComponentMenu("Character/Character Motor")]

// Code is largely a port of the javascript standard first-person character motor

public class CharacterMotor : MonoBehaviour {

	[System.Serializable]
	public class CharacterMotorInput {
		public bool enabled = true;

		[System.NonSerialized]
		public Vector3 moveDirection = Vector3.zero;

		[System.NonSerialized]
		public bool jump = false;

		[System.NonSerialized]
		public bool sprint = false;
	}

	[System.Serializable]
	public class CharacterMotorMovement {
		public Vector3 maxSpeedFSB = new Vector3(10.0f,10.0f,5.0f);
		public Vector3 sprintSpeedMultFSB = new Vector3(1.75f,1.25f,1.25f);

		public AnimationCurve slopeSpeedMultiplier = new AnimationCurve(new Keyframe(-90,1),new Keyframe(0,1),new Keyframe(90,0));

		public float maxGroundAcceleration = 30.0f;
		public float maxAirAcceleration = 20.0f;

		public float gravity = 10.0f;
		public float terminalVelocity = 20.0f;

		[HideInInspector]
		public Vector3 velocity;

		[System.NonSerialized]
		public CollisionFlags collisionFlags;

		[System.NonSerialized]
		public Vector3 hitPoint = new Vector3(Mathf.Infinity,0,0);

		[System.NonSerialized]
		public Vector3 hitPointPrev = Vector3.zero;
	}

	[System.Serializable]
	public class CharacterMotorJumping {
		public bool enabled = true;

		public float baseHeight = 1.0f;
		public float extraHeight = 4.1f;

		// How much does the character jump out perpendicular to the surface on walkable surfaces?
		// 0 means a fully vertical jump and 1 means fully perpendicular.
		public float perpAmount = 0.0f;

		public float steepPerpAmount = 0.5f;

		// How many times can the character jump without touching solid ground (non-steep)
		public int steepJumps = 1;

		[HideInInspector]
		public int steepJumpsUsed = 0;

		// Within how long a player can press the jump button before actually being grounded
		public float graceTime = 0.2f;

		[System.NonSerialized]
		public bool jumping = false;

		[System.NonSerialized]
		public bool holdingJumpButton = false;

		// How long ago the current jump started
		[System.NonSerialized]
		public float lastJumpTime = 0.0f;

		// How long ago a jump was attempted, to prevent jump spamming
		[System.NonSerialized]
		public float lastAttemptTime = -100.0f;

		[System.NonSerialized]
		public Vector3 jumpDir = Vector3.up;
	}

	[System.Serializable]
	public class CharacterMotorSliding {
		public float slidingSpeed = 15.0f;

		// How much can the player control the sliding direction?
		// If the value is 0.5 the player can slide sideways with half the speed of the downwards sliding speed.
		public float sidewaysControl = 1.0f;

		// How much can the player influence the sliding speed?
		// If the value is 0.5 the player can speed the sliding up to 150% or slow it down to 50%.
		public float speedControl = 0.4f;
	}

	public CharacterMotorInput input = new CharacterMotorInput();
	public CharacterMotorMovement movement = new CharacterMotorMovement();
	public CharacterMotorJumping jumping = new CharacterMotorJumping();
	public CharacterMotorSliding sliding = new CharacterMotorSliding();

	[System.NonSerialized]
	public bool grounded = true;

	[System.NonSerialized]
	public bool solidFooting = true;

	[System.NonSerialized]
	public Vector3 groundNormal = Vector3.zero;

	[System.NonSerialized]
	private Vector3 groundNormalPrev = Vector3.zero;

	[System.NonSerialized]
	public CharacterController controller;

	private void Awake() {
		controller = GetComponent<CharacterController>();
	}

	private void Update() {
		Vector3 velocity = movement.velocity;
		velocity = applyInputVelocityChange(velocity);
		velocity = applyGravityAndJumping(velocity);

		// for calculating velocity later
		Vector3 positionPrev = transform.position;

		Vector3 curMovementOffset = velocity * Time.deltaTime;

		// Find out how much we need to push towards the ground to avoid loosing grouning
		// when walking down a step or over a sharp change in slope.
		float pushDownOffset = Mathf.Max(controller.stepOffset,new Vector3(curMovementOffset.x,0,curMovementOffset.z).magnitude);
		if(grounded) {
			curMovementOffset -= pushDownOffset * Vector3.up;
		}

		groundNormal = Vector3.zero;

		// move the character
		movement.collisionFlags = controller.Move(curMovementOffset);

		groundNormalPrev = groundNormal;
		movement.hitPointPrev = movement.hitPoint;

		// calculate our velocity based on how much we actually moved
		Vector3 oldHVelocity = new Vector3(velocity.x,0,velocity.z);
		movement.velocity = (transform.position - positionPrev) / Time.deltaTime;
		Vector3 newHVelocity = new Vector3(movement.velocity.x,0,movement.velocity.z);

		// The CharacterController can be moved in unwanted directions when colliding with things.
		// We want to prevent this from influencing the recorded velocity.
		if(oldHVelocity == Vector3.zero) {
			movement.velocity = new Vector3(0,movement.velocity.y,0);
		} else {
			float projectedNewVelocity = Vector3.Dot(newHVelocity,oldHVelocity) / oldHVelocity.sqrMagnitude;
			// VERY IMPORTANT: if we are currently on a steep slope we need to retain the old vertical speed, to avoid the strange wall-climbing bug
			movement.velocity = oldHVelocity * Mathf.Clamp01(projectedNewVelocity) + (tooSteep() ? velocity.y : movement.velocity.y) * Vector3.up;
		}

		if(movement.velocity.y < velocity.y - 0.001) {
			if(movement.velocity.y < 0) {
				// Something is forcing the CharacterController down faster than it should.
				// Ignore this
				movement.velocity.y = velocity.y;
			} else {
				// The upwards movement of the CharacterController has been blocked.
				// This is treated like a ceiling collision - stop further jumping here.
				jumping.holdingJumpButton = false;
			}
		}

		if(grounded && !testGrounded()) {
			// lost grounding somehow

			grounded = false;

			SendMessage("OnFall",SendMessageOptions.DontRequireReceiver);

			// We pushed the character down to ensure it would stay on the ground if there was any.
			// But there wasn't so now we cancel the downwards offset to make the fall smoother.
			transform.position += pushDownOffset * Vector3.up;
		} else if(!grounded && testGrounded()) {
			// landed on something

			grounded = true;
			jumping.jumping = false;

			SendMessage("OnLand",SendMessageOptions.DontRequireReceiver);
		}

		solidFooting = grounded && !tooSteep();
		if(solidFooting) {
			jumping.steepJumpsUsed = 0;
		}
	}

	// Callbacks

	public void OnControllerColliderHit(ControllerColliderHit hit) {
		if(hit.normal.y > 0 && hit.normal.y > groundNormal.y && hit.moveDirection.y < 0) {
			if((hit.point - movement.hitPointPrev).sqrMagnitude > 0.001 || groundNormalPrev == Vector3.zero)
				groundNormal = hit.normal;
			else
				groundNormal = groundNormalPrev;

			movement.hitPoint = hit.point;
		}
	}

	// Public accessor/helper funcitons

	public bool tooSteep() {
		return groundNormal.y <= Mathf.Cos(controller.slopeLimit * Mathf.Deg2Rad);
	}

	public bool isGrounded() {
		return grounded;
	}

	public bool isSliding() {
		return isGrounded() && tooSteep();
	}

	public bool isJumping() {
		return jumping.jumping;
	}

	public bool isTouchingCeiling() {
		return (movement.collisionFlags & CollisionFlags.CollidedAbove) != 0;
	}

	public float calculateJumpVerticalSpeed(float height) {
		return Mathf.Sqrt(2 * height * movement.gravity);
	}

	// because movement speed is dependant on direction
	public float maxSpeedInDirection(Vector3 dir) {
		if(dir == Vector3.zero) {
			return 0;
		} else {
			Vector3 msFSB = new Vector3();
			msFSB.x = movement.maxSpeedFSB.x * (input.sprint ? movement.sprintSpeedMultFSB.x : 1.0f);
			msFSB.y = movement.maxSpeedFSB.y * (input.sprint ? movement.sprintSpeedMultFSB.y : 1.0f);
			msFSB.z = movement.maxSpeedFSB.z * (input.sprint ? movement.sprintSpeedMultFSB.z : 1.0f);
			float zAxisEllipseMultiplier = (dir.z > 0 ? msFSB.x : msFSB.z) / msFSB.y;
			Vector3 temp = new Vector3(dir.x,0,dir.z / zAxisEllipseMultiplier).normalized;
			float length = new Vector3(temp.x,0,temp.z * zAxisEllipseMultiplier).magnitude * msFSB.y;
			return length;
		}
	}

	public void setVelocity(Vector3 velocity) {
		grounded = false;
		movement.velocity = velocity;
		SendMessage("OnExternalVelocity");
	}

	// Private helper functions

	private Vector3 applyInputVelocityChange(Vector3 velocity) {
		if(!input.enabled) input.moveDirection = Vector3.zero;

		Vector3 desiredVelocity = Vector3.zero;
		if(grounded && tooSteep()) {
			// direction to slide in
			desiredVelocity = new Vector3(groundNormal.x,0,groundNormal.z).normalized;

			Vector3 projectedMoveDir = Vector3.Project(input.moveDirection,desiredVelocity);

			// Add the sliding direction, the spped control, and the sideways control vectors
			desiredVelocity = desiredVelocity + projectedMoveDir * sliding.speedControl + (input.moveDirection - projectedMoveDir) * sliding.sidewaysControl;

			desiredVelocity *= sliding.slidingSpeed;
		} else {
			Vector3 localDir = transform.InverseTransformDirection(input.moveDirection);
			float maxSpeed = maxSpeedInDirection(localDir);
			if(grounded) {
				// Scale max speed on slopes based on slope speed multipler curve.
				float angle = Mathf.Asin(movement.velocity.normalized.y) * Mathf.Rad2Deg;
				maxSpeed *= movement.slopeSpeedMultiplier.Evaluate(angle);
			}
			desiredVelocity = transform.TransformDirection(localDir * maxSpeed);
		}

		if(grounded) {
			Vector3 sideways = Vector3.Cross(Vector3.up,desiredVelocity);
			desiredVelocity = Vector3.Cross(sideways,groundNormal).normalized * desiredVelocity.magnitude;
		} else {
			velocity.y = 0;
		}

		// Enforce max velocity change
		float maxVelocityChange = (grounded ? movement.maxGroundAcceleration : movement.maxAirAcceleration) * Time.deltaTime;
		Vector3 velocityChangeVector = desiredVelocity - velocity;
		if(velocityChangeVector.sqrMagnitude > maxVelocityChange * maxVelocityChange) {
			velocityChangeVector = velocityChangeVector.normalized * maxVelocityChange;
		}

		if(grounded || input.enabled) velocity += velocityChangeVector;

		if(grounded) {
			// the character controller will automatically adjust the position if going uphill
			// this is not true for going downhill though, so ensure that contact with the ground is maintained
			velocity.y = Mathf.Min(velocity.y,0);
		}

		return velocity;
	}

	private Vector3 applyGravityAndJumping(Vector3 velocity) {
		if(!input.jump || !input.enabled) {
			jumping.holdingJumpButton = false;
			jumping.lastAttemptTime = -100;
		}

		if(input.jump && jumping.lastAttemptTime < 0 && input.enabled) jumping.lastAttemptTime = Time.time;

		if(grounded) {
			velocity.y = Mathf.Min(0,velocity.y) - movement.gravity * Time.deltaTime;
		} else {
			velocity.y = movement.velocity.y - movement.gravity * Time.deltaTime;

			// While actually jumping gravity is not applied to make the jump height easily configurable
			if(jumping.jumping && jumping.holdingJumpButton) {
				float jumpDuration = jumping.lastJumpTime + (jumping.extraHeight / calculateJumpVerticalSpeed(jumping.baseHeight));
				if(Time.time < jumpDuration) {
					// negate the gravity in the direction of jumpDir
					velocity += jumping.jumpDir * movement.gravity * Time.deltaTime;
				}
			}

			// apply terminal velocity cap
			velocity.y = Mathf.Max(velocity.y,-movement.terminalVelocity);
		}

		if(grounded && (solidFooting || (jumping.steepJumpsUsed < jumping.steepJumps))) {
			if(jumping.enabled && input.enabled && (Time.time - jumping.lastAttemptTime < jumping.graceTime)) {
				if(tooSteep()) jumping.steepJumpsUsed++;

				grounded = false;
				jumping.jumping = true;
				jumping.lastJumpTime = Time.time;
				jumping.lastAttemptTime = -100;
				jumping.holdingJumpButton = true;

				// calculate jump dir
				if(tooSteep()) {
					jumping.jumpDir = Vector3.Slerp(Vector3.up,groundNormal,jumping.steepPerpAmount);
				} else {
					jumping.jumpDir = Vector3.Slerp(Vector3.up,groundNormal,jumping.perpAmount);
				}

				// apply the jumping force
				velocity.y = 0;
				velocity += jumping.jumpDir * calculateJumpVerticalSpeed(jumping.baseHeight);

				SendMessage("OnJump",SendMessageOptions.DontRequireReceiver);
			} else {
				jumping.holdingJumpButton = false;
			}
		}

		return velocity;
	}

	private bool testGrounded() {
		return groundNormal.y > 0.01f;
	}
}