﻿using System;
using UnityEngine;

/*
Messages:
OnWeaponEquip(BaseWeapon weapon);
OnWeaponUnequip(BaseWeapon weapon);

OnWeaponFire(BaseWeapon weapon);
OnTriggerReleased(BaseWeapon weapon);

OnWeaponFullyAutoFire(BaseWeapon weapon);

OnWeaponSemiAutoFire(BaseWeapon weapon);
OnWeaponSemiAutoTriggerHeld(BaseWeapon weapon);
OnWeaponSemiAutoTriggerReleased(BaseWeapon weapon);

OnWeaponBurstBegin(BaseWeapon weapon);
OnWeaponBurstComplete(BaseWeapon weapon);
OnWeaponBurstFire(BaseWeapon weapon);

OnWeaponChargeBegin(BaseWeapon weapon);
OnWeaponCharging(BaseWeapon weapon);
OnWeaponChargeStop(BaseWeapon weapon); // called when charging stops, weather complete or not
OnWeaponChargeComplete(BaseWeapon weapon);
OnWeaponChargeFireInsufficientCharge(BaseWeapon weapon);
OnWeaponChargeFire(BaseWeapon weapon);
*/

public class BaseWeapon : MonoBehaviour {

	[System.Serializable]
	public class FireStyle_FullyAuto {

		[HideInInspector]
		public BaseWeapon parent;

		public float rateOfFire = 1.0f; //maximum number of firings per second
		public float accuracy = 0.0f; //higher values are less accurate

		public float muzzleVelocity = 35.0f;
		public float muzzleVelocityRand = 5.0f;

		public float damageBase = 10.0f;
		public AnimationCurve damageDstFalloff = new AnimationCurve(new Keyframe(0,1),new Keyframe(5,1),new Keyframe(40,0.25f));

		[HideInInspector]
		[SerializeField]
		protected float timerFire = 0.0f;

		public virtual Vector3 calculateFireDirection(Vector3 aimDirection) {
			Vector3 up,right;
			right = Vector3.Cross(aimDirection,(Mathf.Abs(aimDirection.x) > Mathf.Abs(aimDirection.y) ? Vector3.up : Vector3.right));
			right.Normalize();
			up = Vector3.Cross(right,aimDirection);

			Vector2 dirOffset = UnityEngine.Random.insideUnitCircle * accuracy;

			return (aimDirection + right * dirOffset.x + up * dirOffset.y).normalized;
		}

		public virtual bool isFiring() {
			return parent.triggerDown && timerFire <= 0.0f;
		}

		public virtual void update(float dt) {
			if(timerFire > 0.0f) timerFire -= dt;
			if(parent.triggerDown && parent.canFire() && timerFire <= 0.0f) {
				timerFire = 1.0f / rateOfFire;
				parent.SendMessage("OnWeaponFire",parent,SendMessageOptions.DontRequireReceiver);
				parent.SendMessage("OnWeaponFullyAutoFire",parent,SendMessageOptions.DontRequireReceiver);
			}
		}
	}

	[System.Serializable]
	public class FireStyle_SemiAuto : FireStyle_FullyAuto {

		[HideInInspector]
		public bool triggerReleased { get; protected set; }

		[HideInInspector]
		public bool triggerReleasedPrev { get; protected set; }

		public FireStyle_SemiAuto() {
			triggerReleased = triggerReleasedPrev = true;
		}

		public override void update(float dt) {
			triggerReleasedPrev = triggerReleased;
			if(!parent.triggerDown || !parent.canFire()) triggerReleased = true;

			if(timerFire > 0.0f) timerFire -= dt;
			if(parent.triggerDown && parent.canFire() && timerFire <= 0.0f && triggerReleased) {
				triggerReleased = false;
				timerFire = 1.0f / rateOfFire;
				parent.SendMessage("OnWeaponFire",parent,SendMessageOptions.DontRequireReceiver);
				parent.SendMessage("OnWeaponSemiAutoFire",parent,SendMessageOptions.DontRequireReceiver);
			} else if((!parent.triggerDown || !parent.canFire()) && !triggerReleasedPrev) {
				parent.SendMessage("OnWeaponSemiAutoTriggerRelease",parent,SendMessageOptions.DontRequireReceiver);
			} else if(parent.triggerDown && !triggerReleasedPrev) {
				parent.SendMessage("OnWeaponSemiAutoTriggerHeld",parent,SendMessageOptions.DontRequireReceiver);
			}
		}
	}

	[System.Serializable]
	public class FireStyle_Burst : FireStyle_SemiAuto {
		public int burstNum = 3; //number of fires per burst
		public float burstRate = 20.0f; //rate of fire during a burst

		[HideInInspector]
		[SerializeField]
		protected float timerBurst = 0.0f;

		[HideInInspector]
		[SerializeField]
		protected int counterBurst = 0;

		public override void update(float dt) {
			triggerReleasedPrev = triggerReleased;
			if(!parent.triggerDown || !parent.canFire()) triggerReleased = true;

			if(timerFire > 0.0f) timerFire -= dt;
			if(parent.triggerDown && parent.canFire() && triggerReleased && timerFire <= 0.0f) {
				if(counterBurst == 0) {
					triggerReleased = false;
					counterBurst = burstNum;
					parent.SendMessage("OnWeaponBurstBegin",parent,SendMessageOptions.DontRequireReceiver);
				} else {
					if(timerBurst > 0.0f) timerBurst -= dt;
					if(timerBurst <= 0.0f) {
						counterBurst -= 1;

						parent.SendMessage("OnWeaponFire",parent,SendMessageOptions.DontRequireReceiver);
						parent.SendMessage("OnWeaponBurstFire",parent,SendMessageOptions.DontRequireReceiver);

						if(counterBurst == 0) {
							timerFire = 1.0f / rateOfFire;
							parent.SendMessage("OnWeaponBurstComplete",parent,SendMessageOptions.DontRequireReceiver);
						} else {
							timerBurst = 1.0f / burstRate;
						}
					}
				}
			}
		}
	}

	[System.Serializable]
	public class FireStyle_Charge : FireStyle_SemiAuto {
		public float chargeTime = 1.0f; //seconds to fully charge
		public float chargeMinimum = 0.1f; //percent of charge required to actually fire

		[HideInInspector]
		[SerializeField]
		protected float timerCharge = 0.0f;

		private bool hasSentChargeComplete;

		public float getChargePercent() {
			return Mathf.Clamp01(timerCharge / chargeTime);
		}

		public override void update(float dt) {
			triggerReleasedPrev = triggerReleased;

			if(timerFire > 0.0f) timerFire -= dt;

			if(parent.canFire() && timerFire <= 0.0f) {
				if(!parent.triggerDown && !triggerReleased) {
					//charging completed, fire
					timerFire = 1.0f / rateOfFire;

					if(getChargePercent() != 1) {
						parent.SendMessage("OnWeaponChargeStop",parent,SendMessageOptions.DontRequireReceiver);
					}

					if(getChargePercent() >= chargeMinimum) {
						parent.SendMessage("OnWeaponFire",parent,SendMessageOptions.DontRequireReceiver);
						parent.SendMessage("OnWeaponChargeFire",parent,SendMessageOptions.DontRequireReceiver);
					} else {
						parent.SendMessage("OnWeaponChargeFireInsufficientCharge",parent,SendMessageOptions.DontRequireReceiver);
					}
				} else if(parent.triggerDown) {
					if(triggerReleased) {
						//charging beginning
						triggerReleased = false;
						hasSentChargeComplete = false;
						timerCharge = 0.0f;
						parent.SendMessage("OnWeaponChargeBegin",parent,SendMessageOptions.DontRequireReceiver);
					}
					if(getChargePercent() == 1) {
						if(!hasSentChargeComplete) {
							hasSentChargeComplete = true;
							//charging complete, do not fire yet
							parent.SendMessage("OnWeaponChargeStop",parent,SendMessageOptions.DontRequireReceiver);
							parent.SendMessage("OnWeaponChargeComplete",parent,SendMessageOptions.DontRequireReceiver);
						}
					} else {
						//charing in progress
						timerCharge += dt;
						parent.SendMessage("OnWeaponCharging",parent,SendMessageOptions.DontRequireReceiver);
					}
				}

				if(!parent.triggerDown || !parent.canFire()) triggerReleased = true;
			}
		}
	}

	public Transform muzzleTrans = null;

	[HideInInspector]
	public Vector3 projectilePosition = Vector3.zero;

	[HideInInspector]
	public Vector3 projectileDirection = Vector3.zero;

	[HideInInspector]
	public bool triggerDown = false;

	[HideInInspector]
	public FireStyle_FullyAuto fireStyleCur = null;

	public Animator handAnimatior = null;

	private bool _equipped = false;
	[HideInInspector]
	public bool equipped {
		get {
			return _equipped;
		}
		set {
			if(_equipped && !value) {
				SendMessage("OnWeaponUnequip",this,SendMessageOptions.DontRequireReceiver);
				_equipped = false;
			} else if(!_equipped && value) {
				SendMessage("OnWeaponEquip",this,SendMessageOptions.DontRequireReceiver);
				_equipped = true;
			}
		}
	}

	[HideInInspector]
	public GameObject wielder = null;

	[HideInInspector]
	public Func<bool> canFire = () => false;

	[HideInInspector]
	public bool isFiring {
		get {
			if(fireStyleCur != null) return fireStyleCur.isFiring();
			return false;
		}
	}

	public void resetProjectileTransform() {
		projectilePosition = muzzleTrans.position;
		projectileDirection = muzzleTrans.forward;
	}

	private void Reset() {
		canFire = () => equipped;
	}

	private void Update() {
		if(fireStyleCur != null) {
			if(!equipped) {
				resetProjectileTransform();
			}

			fireStyleCur.parent = this;
			fireStyleCur.update(Time.deltaTime);
		}
	}

	private void OnWeaponEquip(BaseWeapon weapon) {
		if(handAnimatior != null) {
			handAnimatior.Update(99999999); // work-around to a Unity bug
			handAnimatior.SetTrigger("OnEquip");
		}
	}

	private void OnWeaponUnequip(BaseWeapon weapon) {
		if(handAnimatior != null) {
			handAnimatior.Update(99999999); // work-around to a Unity bug
			handAnimatior.ResetTrigger("OnEquip");
		}

		// ensure that the proper messages are sent
		weapon.triggerDown = false;
		if(weapon.fireStyleCur != null) {
			fireStyleCur.parent = this;
			fireStyleCur.update(0);
		}
	}
}