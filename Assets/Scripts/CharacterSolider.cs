﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CharacterSolider : MonoBehaviour {
	public GameObject prefabBloodSpurt = null;

	public BaseWeapon myWeapon = null;
	public Transform myProjectileAnchor = null;
	public RAIN.Core.AIRig myAIRig = null;

	private void Start() {
		if(myWeapon != null) {
			myWeapon.wielder = gameObject;
			myWeapon.equipped = true;

			myWeapon.triggerDown = false;
		}
	}

	private void LateUpdate() {
		GameObject ai_attack_target = null;
		RAIN.Entities.Aspects.VisualAspect ai_attack_target_asp = null;
		bool ai_attacking = false;

		if(myAIRig != null) {
			ai_attack_target = myAIRig.AI.WorkingMemory.GetItem<GameObject>("attack_target");
			ai_attack_target_asp = myAIRig.AI.WorkingMemory.GetItem<RAIN.Entities.Aspects.VisualAspect>("attack_target_asp");
			ai_attacking = myAIRig.AI.WorkingMemory.GetItem<bool>("attack_attacking");

			if(ai_attack_target != null) {
				myAIRig.AI.WorkingMemory.SetItem<float>("attack_target_curdst",(transform.position - ai_attack_target.transform.position).magnitude);
			} else {
				myAIRig.AI.WorkingMemory.SetItem<float>("attack_target_curdst",0);
			}
		}

		if(myWeapon != null && myProjectileAnchor != null) {
			myWeapon.projectilePosition = myProjectileAnchor.position;
			if(ai_attack_target_asp != null) {
				myWeapon.projectileDirection = (ai_attack_target_asp.Position - myWeapon.projectilePosition).normalized;
			}
			myWeapon.triggerDown = ai_attacking;
		}
	}

	private void OnApplyDamage(DamageSource damage) {
		if(prefabBloodSpurt != null && damage.name == "Projectile") {
			GameObject obj = Instantiate(prefabBloodSpurt,damage.inflictionLocation,Quaternion.LookRotation(damage.inflictionNormal)) as GameObject;
			obj.transform.parent = SDynamicObjects.instance.transform;
			DelayedAction.EnqueAction(obj,2.0f,() => {
				Destroy(obj);
			});
		}
	}

	private void OnDie() {
		Destroy(gameObject);
	}
}