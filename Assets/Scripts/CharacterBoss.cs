﻿using UnityEngine;

public class CharacterBoss : MonoBehaviour {
	public BaseWeapon myWeapon = null;
	public Transform myProjectileAnchor = null;

	public UISlider myHealthSlider = null;

	private void Start() {
		if(myWeapon != null) {
			myWeapon.wielder = gameObject;
			myWeapon.equipped = true;

			myWeapon.triggerDown = false;
		}

		if(myHealthSlider != null) myHealthSlider.gameObject.SetActive(true);
	}

	private void LateUpdate() {
		GameObject ai_attack_target = GameObject.FindGameObjectWithTag("Player");

		if(myWeapon != null && myProjectileAnchor != null) {
			myWeapon.projectilePosition = myProjectileAnchor.position;
			if(ai_attack_target != null) {
				Vector3 target = ai_attack_target.transform.position + Vector3.up * 1.0f;
				myWeapon.projectileDirection = (target - myWeapon.projectilePosition).normalized;
			}
			myWeapon.triggerDown = true;
		}
	}

	private void OnDie() {
		Destroy(gameObject);
		if(myHealthSlider != null) myHealthSlider.gameObject.SetActive(false);
	}

	private void OnHealthChange(CharacterHealth health) {
		if(myHealthSlider != null) myHealthSlider.sliderValue = Mathf.Clamp01(health.healthCur / health.healthMax);
	}
}