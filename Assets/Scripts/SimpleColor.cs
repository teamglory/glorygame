﻿using UnityEngine;

[ExecuteInEditMode]
public class SimpleColor : MonoBehaviour {
	public Color objectColor = new Color(0.5f,0.5f,0.5f,1.0f);

	[HideInInspector]
	[SerializeField]
	private Material tempmaterial = null;

	// Update is called once per frame
	private void Update() {
		if(tempmaterial == null) {
			tempmaterial = new Material(Shader.Find("Diffuse"));
		}
		if(tempmaterial.color != objectColor) {
			tempmaterial.color = objectColor;
			renderer.material = tempmaterial;
		}
	}

	private void OnDestroy() {
		if(tempmaterial != null) {
			//UnityEditor.AssetDatabase.DeleteAsset(UnityEditor.AssetDatabase.GetAssetPath(tempmaterial));
		}
	}
}