﻿using UnityEngine;
using System.Collections;

public class TriggerPlayMusic : MonoBehaviour {
	public AudioClip music = null;
	float musicVolume = 0.5f;

	public float fadeOutTime = 5.0f;
	public float fadeInTime = 5.0f;

	private void OnTriggerEnter(Collider col) {
		if(music != null && col.gameObject.tag == "Player") {
			MusicManager.play(music,fadeOutTime,fadeInTime);
			MusicManager.setVolume(musicVolume,fadeInTime);
			Physics.IgnoreCollision(col,collider);
		}
	}
}
