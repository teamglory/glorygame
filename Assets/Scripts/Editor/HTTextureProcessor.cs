﻿using UnityEditor;
using UnityEngine;

public class HTTextureProcessor : AssetPostprocessor {

	const string suffixNormalMap = "_NRM";

	void OnPostProcessTexture(Texture2D texture) {
		if(texture.name.Contains(suffixNormalMap)) {
			string path = AssetDatabase.GetAssetPath(texture);
			TextureImporter timp = AssetImporter.GetAtPath(path) as TextureImporter;
			if(timp != null) {
				timp.normalmap = true;
				AssetDatabase.ImportAsset(path,ImportAssetOptions.ForceUpdate);
			}
		}
	}
}
