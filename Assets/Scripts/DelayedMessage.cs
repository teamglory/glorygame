﻿using UnityEngine;

public class DelayedMessage : MonoBehaviour {
	public float timeLeft;

	public string methodName;
	public object methodArgument = null;

	public SendMessageOptions messageOptions = SendMessageOptions.RequireReceiver;

	public enum SendType { UP, SELF, DOWN }

	public SendType sendType = SendType.SELF;

	private void Update() {
		timeLeft -= Time.deltaTime;
		if(timeLeft <= 0.0f) {
			switch(sendType) {
				case DelayedMessage.SendType.UP: {
						SendMessageUpwards(methodName,methodArgument,messageOptions);
						break;
					}
				case DelayedMessage.SendType.SELF: {
						SendMessage(methodName,methodArgument,messageOptions);
						break;
					}
				case DelayedMessage.SendType.DOWN: {
						BroadcastMessage(methodName,methodArgument,messageOptions);
						break;
					}
			}
			Destroy(this);
		}
	}

	public static void EnqueMessageUp(GameObject methodTarget,float delay,string methodName) {
		EnqueMessageUp(methodTarget,delay,methodName,null,SendMessageOptions.RequireReceiver);
	}

	public static void EnqueMessageUp(GameObject methodTarget,float delay,string methodName,object methodArgument) {
		EnqueMessageUp(methodTarget,delay,methodName,methodArgument,SendMessageOptions.RequireReceiver);
	}

	public static void EnqueMessageUp(GameObject methodTarget,float delay,string methodName,SendMessageOptions messageOptions) {
		EnqueMessageUp(methodTarget,delay,methodName,null,messageOptions);
	}

	public static void EnqueMessageUp(GameObject methodTarget,float delay,string methodName,object methodArgument,SendMessageOptions messageOptions) {
		_EnqueMessage(methodTarget,delay,methodName,methodArgument,messageOptions,SendType.UP);
	}

	public static void EnqueMessage(GameObject methodTarget,float delay,string methodName) {
		EnqueMessage(methodTarget,delay,methodName,null,SendMessageOptions.RequireReceiver);
	}

	public static void EnqueMessage(GameObject methodTarget,float delay,string methodName,object methodArgument) {
		EnqueMessage(methodTarget,delay,methodName,methodArgument,SendMessageOptions.RequireReceiver);
	}

	public static void EnqueMessage(GameObject methodTarget,float delay,string methodName,SendMessageOptions messageOptions) {
		EnqueMessage(methodTarget,delay,methodName,null,messageOptions);
	}

	public static void EnqueMessage(GameObject methodTarget,float delay,string methodName,object methodArgument,SendMessageOptions messageOptions) {
		_EnqueMessage(methodTarget,delay,methodName,methodArgument,messageOptions,SendType.SELF);
	}

	public static void EnqueMessageDown(GameObject methodTarget,float delay,string methodName) {
		EnqueMessageDown(methodTarget,delay,methodName,null,SendMessageOptions.RequireReceiver);
	}

	public static void EnqueMessageDown(GameObject methodTarget,float delay,string methodName,object methodArgument) {
		EnqueMessageDown(methodTarget,delay,methodName,methodArgument,SendMessageOptions.RequireReceiver);
	}

	public static void EnqueMessageDown(GameObject methodTarget,float delay,string methodName,SendMessageOptions messageOptions) {
		EnqueMessageDown(methodTarget,delay,methodName,null,messageOptions);
	}

	public static void EnqueMessageDown(GameObject methodTarget,float delay,string methodName,object methodArgument,SendMessageOptions messageOptions) {
		_EnqueMessage(methodTarget,delay,methodName,methodArgument,messageOptions,SendType.DOWN);
	}

	private static void _EnqueMessage(GameObject methodTarget,float delay,string methodName,object methodArgument,SendMessageOptions messageOptions,SendType sendType) {
		var newMessage = methodTarget.AddComponent<DelayedMessage>();
		newMessage.timeLeft = delay;
		newMessage.methodName = methodName;
		newMessage.methodArgument = methodArgument;
		newMessage.messageOptions = messageOptions;
		newMessage.sendType = sendType;
	}
}