﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {
	public AudioClip startingMusic = null;
	float startingMusicVolume = 0.5f;

	void Start () {
		if(startingMusic != null) MusicManager.play(startingMusic,5,5);
		MusicManager.setVolume(startingMusicVolume);
	}
	
	void Update () {
	
	}
}
