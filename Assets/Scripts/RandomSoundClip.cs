﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class RandomSoundClip {
	[System.Serializable]
	public class SoundEntry {
		public AudioClip sound = null;
		public float weight = 1.0f;
	}

	public SoundEntry[] soundList;

	public AudioClip randomClip {
		get {
			float total = 0.0f;
			foreach(var entry in soundList) {
				if(entry.sound != null) total += entry.weight;
			}
			float rnd = UnityEngine.Random.value * total;
			float cur = 0.0f;
			foreach(var entry in soundList) {
				if(entry.sound != null) {
					if(cur + entry.weight >= rnd) return entry.sound;
					cur += entry.weight;
				}
			}
			return null;
		}
	}
}
