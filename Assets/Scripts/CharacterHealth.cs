using UnityEngine;

/*
Messages:
OnApplyDamage(DamageSource damage); //damage's contents can be modified to change outcome of the damage application
OnHealthChange(CharacterHealth item);
OnDie();
*/

public class CharacterHealth : MonoBehaviour {
	public float healthMax = 100.0f;
	public float healthCur = -1.0f;
	public float regenRate = 2.5f;

	public float regenMult = 1.0f;

	public float healthPrev { get; private set; }

	public void applyDamage(DamageSource damage) {
		damage.target = this;
		SendMessage("OnApplyDamage",damage,SendMessageOptions.DontRequireReceiver);
		healthCur -= damage.strength;
	}

	private void Reset() {
		if(healthCur == -1.0f) healthCur = healthMax;
	}

	private void Start() {
		healthPrev = healthCur;
	}

	private void Update() {
		healthCur = Mathf.Min(healthMax,healthCur + regenRate * regenMult * Time.deltaTime);
	}

	private void LateUpdate() {
		healthCur = Mathf.Min(healthMax,healthCur);
		if(healthCur != healthPrev) {
			SendMessage("OnHealthChange",this,SendMessageOptions.DontRequireReceiver);
			if(healthCur <= 0.0f && healthPrev > 0.0f) SendMessage("OnDie");
		}
		healthPrev = healthCur;
	}
}