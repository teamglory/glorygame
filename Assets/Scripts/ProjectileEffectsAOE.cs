﻿using UnityEngine;

[RequireComponent(typeof(Projectile))]
public class ProjectileEffectsAOE : MonoBehaviour {

	[System.Serializable]
	public class Sound {
		public RandomSoundClip impact;
	}

	public Sound sound;

	public GameObject debrisPrefab = null;

	public float aoeRadius = 2.0f;
	public AnimationCurve aoeFalloff = new AnimationCurve(new Keyframe(0,1),new Keyframe(2,0.0f));

	//public float explosionForce = 20.0f;

	private void OnProjectileCollision(RaycastHit hit) {
		Projectile asProjectile = GetComponent<Projectile>();

		if(asProjectile != null && asProjectile.destruction.destroy) {
			HTUtil.SoundPlayAtLocationEx(hit.point,audio,sound.impact.randomClip);

			if(debrisPrefab != null) {
				Vector3 debrisForward = hit.normal;
				Vector3 debrisPos = hit.point + debrisForward * 0.05f;
				Quaternion debrisRot = Quaternion.LookRotation(debrisForward);
				GameObject debris = (GameObject)Instantiate(debrisPrefab,debrisPos,debrisRot);
				debris.transform.parent = SDynamicObjects.instance.transform;
			}

			var aoehits = Physics.OverlapSphere(transform.position,aoeRadius);

			foreach(var aoehit in aoehits) {
				CharacterHealth otherHealth = aoehit.collider.gameObject.GetComponent<CharacterHealth>();
				if(otherHealth != null) {
					RaycastHit rchit;
					Vector3 rcpos = hit.point + hit.normal * 0.1f;
					Vector3 rcdir = (aoehit.bounds.center - rcpos).normalized;
					if(aoehit.Raycast(new Ray(rcpos,rcdir),out rchit,aoeRadius)) {
						float aoeDst = rchit.distance / aoeRadius;
						var projectile = GetComponent<Projectile>();
						float damage = projectile.damage.damageBase * projectile.damage.damageDstFalloff.Evaluate(projectile.kinimatics.distanceTraveled) * aoeFalloff.Evaluate(aoeDst);
						otherHealth.applyDamage(new DamageSource("AOE",damage,gameObject,rchit.point,rchit.normal));
					}
				}
			}
		}
	}
}