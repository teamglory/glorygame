﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HTExtensionMethods {

	//function by vexe: http://answers.unity3d.com/questions/530178/how-to-get-a-component-from-an-object-and-add-it-t.html
	public static T SetAsCopyOf<T>(this Component comp,T other) where T : Component {
		Type type = comp.GetType();
		if(type != other.GetType()) return null; // type mis-match
		System.Reflection.BindingFlags flags =
			System.Reflection.BindingFlags.Public |
			System.Reflection.BindingFlags.NonPublic |
			System.Reflection.BindingFlags.Instance |
			System.Reflection.BindingFlags.Default |
			System.Reflection.BindingFlags.DeclaredOnly;

		System.Reflection.PropertyInfo[] pinfos = type.GetProperties(flags);
		foreach(var pinfo in pinfos) {
			if(pinfo.CanWrite) {
				try {
					//ignorance list
					if(type == typeof(AudioSource))
					if(pinfo.Name == "minVolume" ||
					   pinfo.Name == "maxVolume" ||
					   pinfo.Name == "rolloffFactor") continue;
					pinfo.SetValue(comp,pinfo.GetValue(other,null),null);
				} catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
			}
		}
		System.Reflection.FieldInfo[] finfos = type.GetFields(flags);
		foreach(var finfo in finfos) {
			finfo.SetValue(comp,finfo.GetValue(other));
		}
		return comp as T;
	}

	//function by vexe: http://answers.unity3d.com/questions/530178/how-to-get-a-component-from-an-object-and-add-it-t.html
	public static T AddComponent<T>(this GameObject go,T toAdd) where T : Component {
		return go.AddComponent<T>().SetAsCopyOf(toAdd) as T;
	}

	public static Transform FindChildRecursive(this Transform t,string name) {
		Transform ret = null;
		HTUtil.ObjectHierarchyBredthFirst(t.gameObject,(GameObject obj) => {
			if(obj.name == name) {
				ret = obj.transform;
				return HTUtil.AlgorithmAction.BREAK;
			}
			return HTUtil.AlgorithmAction.CONTINUE;
		});
		return ret;
	}
}

public class HTUtil {

	public enum AlgorithmAction { CONTINUE, SKIP, BREAK }

	public static void ObjectHierarchyBredthFirst(GameObject root,Func<GameObject,AlgorithmAction> visit) {
		if(root == null) return;
		Queue<GameObject> queue = new Queue<GameObject>();
		queue.Enqueue(root);
		while(queue.Count > 0) {
			var node = queue.Dequeue();
			AlgorithmAction action = visit(node);
			if(action == AlgorithmAction.BREAK) break;
			if(action == AlgorithmAction.CONTINUE) for(int i = 0; i < node.transform.childCount; i++) {
					queue.Enqueue(node.transform.GetChild(i).gameObject);
				}
		}
	}

	public static Bounds CalculateCompositeBounds(GameObject rootWRigidBody) {
		bool foundfirst = false;
		Bounds totalbounds = new Bounds();
		HTUtil.ObjectHierarchyBredthFirst(rootWRigidBody,(GameObject obj) => {
			//if the object has a rigidbody then it is independent
			if(obj != rootWRigidBody && obj.rigidbody != null) return HTUtil.AlgorithmAction.SKIP;

			if(obj.collider != null) {
				if(!foundfirst) {
					foundfirst = true;
					totalbounds = obj.collider.bounds;
				} else {
					totalbounds.Encapsulate(obj.collider.bounds);
				}
			}
			return HTUtil.AlgorithmAction.CONTINUE;
		});
		return totalbounds;
	}

	public static GameObject SoundPlayAtLocationEx(Vector3 position,AudioClip clip) {
		if(clip == null) return null;

		GameObject tmpobj = new GameObject("_slaveaudio");
		tmpobj.transform.parent = SDynamicObjects.instance.transform;
		tmpobj.transform.position = position;
		tmpobj.AddComponent<AudioSource>();
		tmpobj.audio.PlayOneShot(clip);
		DelayedAction.EnqueAction(tmpobj,clip.length,() => {
			GameObject.Destroy(tmpobj);
		});
		return tmpobj;
	}

	public static GameObject SoundPlayAtLocationEx(Vector3 position,AudioSource master,AudioClip clip) {
		if(clip == null) return null;

		GameObject tmpobj = new GameObject(master.gameObject.name + "_slaveaudio");
		tmpobj.transform.parent = SDynamicObjects.instance.transform;
		tmpobj.transform.position = position;
		tmpobj.AddComponent(master);
		tmpobj.audio.PlayOneShot(clip);
		DelayedAction.EnqueAction(tmpobj,clip.length,() => {
			GameObject.Destroy(tmpobj);
		});
		return tmpobj;
	}

	public static float PingPong(float tcur,float tmax,float min,float max) {
		float p = Mathf.PingPong(tcur,tmax) / tmax;
		return (p * ((min) - (max)) + (max));
	}
}