﻿using System.Collections.Generic;
using UnityEngine;

/*
Messages:
OnProjectileCollision(RaycastHit hit);
*/

public class Projectile : MonoBehaviour {

	[System.Serializable]
	public class Destruction {
		public bool bounceEnabled = true;
		public int maxBounces = 0;

		[HideInInspector]
		[SerializeField]
		public int counterBounces = 0;

		public bool timeEnabled = true;
		public float timeMax = 10.0f;

		[HideInInspector]
		public bool destroy = false;
	}

	[System.Serializable]
	public class Damage {
		public float damageBase = 10.0f;
		public AnimationCurve damageDstFalloff = new AnimationCurve(new Keyframe(5,1),new Keyframe(40,0.25f));
	}

	[System.Serializable]
	public class Graphics {
		public bool useDifferedGraphics = true;

		public bool faceForward = true;
		public float syncLerpStep = 5.0f;
		public Transform targetTrans = null;
	}

	[System.Serializable]
	public class Kinimatics {

		[Tooltip("This DISABLES all kinimatic functions of this script, including OnProjectileCollision()")]
		public bool useCustomKinimatics = true;

		[Tooltip("Only if custom kinimatics is DISABLED")]
		public float ignorancePollRadius = 5.0f;

		[Tooltip("Only if custom kinimatics is ENABLED")]
		public float gravityMultiplier = 1.0f;

		[Tooltip("Only if custom kinimatics is ENABLED")]
		public float bounceDampen = 0.3f;

		[HideInInspector]
		public Vector3 velocity = Vector3.zero;

		[HideInInspector]
		public float distanceTraveled = 0.0f;

		[HideInInspector]
		public List<Collider> phyColliders = new List<Collider>();
	}

	public Destruction destruction;
	public Graphics graphics;

	public Kinimatics kinimatics;

	[HideInInspector]
	public Damage damage;

	[HideInInspector]
	public GameObject owner = null;

	private void Reset() {
		graphics.targetTrans = transform.FindChild("Graphics");
	}

	private void Awake() {
		if(!kinimatics.useCustomKinimatics) {
			if(graphics.targetTrans == null) graphics.targetTrans = transform.FindChild("Graphics");
			HTUtil.ObjectHierarchyBredthFirst(gameObject,(GameObject obj) => {
				//if the object has a rigidbody then it is independent
				if(obj != gameObject && obj.rigidbody != null) return HTUtil.AlgorithmAction.SKIP;

				if(obj.collider != null && !obj.collider.isTrigger) {
					kinimatics.phyColliders.Add(obj.collider);
				}
				return HTUtil.AlgorithmAction.CONTINUE;
			});

			CheckCollisionIgnorance();
		}
	}

	private void Start() {
		if(destruction.timeEnabled) DelayedMessage.EnqueMessage(gameObject,destruction.timeMax,"OnProjectileExpire");
	}

	private void Update() {
		if(graphics.targetTrans && graphics.useDifferedGraphics) {
			if(graphics.faceForward) {
				Quaternion rot = new Quaternion();
				if(kinimatics.velocity.sqrMagnitude > 0.001)
					rot.SetLookRotation(kinimatics.velocity.normalized,Vector3.up);
				else
					rot = transform.rotation;
				graphics.targetTrans.rotation = Quaternion.Slerp(graphics.targetTrans.rotation,rot,graphics.syncLerpStep * Time.deltaTime);
			}

			graphics.targetTrans.localPosition = Vector3.Lerp(graphics.targetTrans.localPosition,Vector3.zero,graphics.syncLerpStep * Time.deltaTime);
		}
	}

	private void FixedUpdate() {
		if(kinimatics.useCustomKinimatics) {
			RaycastHit[] hits;
			hits = Physics.RaycastAll(new Ray(transform.position,kinimatics.velocity.normalized),kinimatics.velocity.magnitude * Time.fixedDeltaTime);

			transform.position += kinimatics.velocity * Time.fixedDeltaTime;
			kinimatics.distanceTraveled += kinimatics.velocity.magnitude * Time.fixedDeltaTime;
			kinimatics.velocity += Physics.gravity * kinimatics.gravityMultiplier * Time.fixedDeltaTime;

			foreach(var hit in hits) {
				if(!ShouldCollideWith(hit.transform.gameObject)) continue;

				// collision accepted

				transform.position = hit.point + 0.1f * hit.normal;
				kinimatics.velocity = Vector3.Reflect(kinimatics.velocity,hit.normal) * (1.0f - kinimatics.bounceDampen);
				SendProjectileCollision(hit);

				break;
			}
		} else {
			if(rigidbody != null) kinimatics.distanceTraveled += rigidbody.velocity.magnitude * Time.fixedDeltaTime;
			CheckCollisionIgnorance();
		}
	}

	private void LateUpdate() {
		if(destruction.destroy) {
			Destroy(gameObject);
		}
	}

	private void SendProjectileCollision(RaycastHit hit) {
		if(destruction.bounceEnabled) {
			destruction.counterBounces++;
			if(destruction.counterBounces > destruction.maxBounces) destruction.destroy = true;
		}
		SendMessage("OnProjectileCollision",hit,SendMessageOptions.DontRequireReceiver);
	}

	private void OnProjectileExpire() {
		destruction.destroy = true;
	}

	public bool ShouldCollideWith(GameObject go) {
		if(Physics.GetIgnoreLayerCollision(gameObject.layer,go.layer)) return false;

		var asProjectile = go.GetComponent<Projectile>();
		// check one level higher
		if(asProjectile == null && go.transform.parent != null) {
			asProjectile = go.transform.parent.GetComponent<Projectile>();
		}
		if((asProjectile != null && asProjectile.owner == owner) || (go == owner)) {
			return false;
		}

		return true;
	}

	private void CheckCollisionIgnorance() {
		var ignoreCandidiates = Physics.OverlapSphere(transform.position,kinimatics.ignorancePollRadius);
		foreach(var candidate in ignoreCandidiates) {
			if(candidate.enabled) {
				if(ShouldCollideWith(candidate.gameObject)) continue;

				foreach(var pcol in kinimatics.phyColliders) {
					if(pcol.enabled && pcol != candidate) Physics.IgnoreCollision(pcol,candidate);
				}
			}
		}
	}
}

/*
Ignorance code:
	private void Awake() {
		if(graphics.targetTrans == null) graphics.targetTrans = transform.FindChild("Graphics");
		HTUtil.ObjectHierarchyBredthFirst(gameObject,(GameObject obj) => {
			//if the object has a rigidbody then it is independent
			if(obj != gameObject && obj.rigidbody != null) return HTUtil.AlgorithmAction.SKIP;

			if(obj.collider != null && !obj.collider.isTrigger) {
				kinimatics.phyColliders.Add(obj.collider);
			}
			return HTUtil.AlgorithmAction.CONTINUE;
		});

		CheckCollisionIgnorance();
	}

	private void CheckCollisionIgnorance() {
		var ignoreCandidiates = Physics.OverlapSphere(transform.position,ignorePollRadius);
		foreach(var candidate in ignoreCandidiates) {
			if(candidate.enabled) {
				var asProjectile = candidate.gameObject.GetComponent<Projectile>();
				if(asProjectile == null && candidate.gameObject.transform.parent != null) {
					asProjectile = candidate.gameObject.transform.parent.GetComponent<Projectile>();
				}
				if((asProjectile != null && asProjectile.owner == owner) || (candidate.gameObject == owner)) {
					foreach(var pcol in kinimatics.phyColliders) {
						if(pcol.enabled && pcol != candidate) Physics.IgnoreCollision(pcol,candidate);
					}
				}
			}
		}
	}

	private void OnCollisionEnter(Collision collision) {
		if(kinimatics.useRigidBody) {
			if(Time.frameCount != kinimatics.processedCollisionsLFC) {
				kinimatics.processedCollisionsLFC = Time.frameCount;
				kinimatics.processedCollisions.Clear();
			} else {
				if(kinimatics.processedCollisions.Contains(collision.gameObject)) return;
			}

			kinimatics.processedCollisions.Add(collision.gameObject);

			SendMessage("OnProjectileCollision",collision,SendMessageOptions.DontRequireReceiver);
		}
	}
*/