﻿using UnityEngine;

/*
Messages:
OnReloadBegin(BaseGun gun);
OnReloadCancel(BaseGun gun);
OnReloadStop(BaseGun gun);
OnReloadComplete(BaseGun gun);
*/

[RequireComponent(typeof(BaseWeapon))]
public class BaseGun : MonoBehaviour {

	[HideInInspector]
	public BaseWeapon weapon;

	public CharacterAmmo.AmmoType ammoType = CharacterAmmo.AmmoType.RIFLE;

	protected CharacterAmmo ammoSource = null;

	public int magazineMaximum = 12;
	public int magazineCur = 12;

	public bool automaticReload = true;

	public float reloadTime = 3.0f;

	public GameObject muzzleFlashPrefab = null;

	public bool reloading { get; private set; }

	[HideInInspector]
	[SerializeField]
	protected float reloadTimer = 0.0f;

	public float reloadProgress() {
		if(reloading) return (1.0f - (reloadTimer / reloadTime));
		return 0.0f;
	}

	//starts reloading, the reload can be cancelled
	public void reloadBegin() {
		if(!reloading && ammoSource != null && magazineCur < magazineMaximum && ammoSource.ammoCur > 0) {
			reloading = true;
			reloadTimer = reloadTime;
			SendMessage("OnReloadBegin",this,SendMessageOptions.DontRequireReceiver);
		}
	}

	//cancells the reload
	public void reloadCancel() {
		if(reloading) {
			reloading = false;
			reloadTimer = 0.0f;
			SendMessage("OnReloadStop",this,SendMessageOptions.DontRequireReceiver);
			SendMessage("OnReloadCancel",this,SendMessageOptions.DontRequireReceiver);
		}
	}

	//message callback
	private void OnWeaponFire() {
		magazineCur -= 1;

		if(muzzleFlashPrefab != null && weapon.muzzleTrans != null) {
			GameObject flash = (GameObject)Instantiate(muzzleFlashPrefab,weapon.muzzleTrans.position,weapon.muzzleTrans.rotation);
			if(flash != null) flash.transform.parent = transform;
		}
	}

	private void Awake() {
		weapon = GetComponent<BaseWeapon>();
		weapon.canFire = () => weapon.equipped && magazineCur > 0 && !reloading;

		aquireAmmoSource();
	}

	private void Update() {
		if(rigidbody != null) {
			if(weapon.equipped) {
				rigidbody.detectCollisions = false;
				rigidbody.isKinematic = true;
			} else {
				rigidbody.detectCollisions = true;
				rigidbody.isKinematic = false;
			}
		}

		if(weapon.equipped && ammoSource == null) {
			aquireAmmoSource();
		}
		if(reloading) {
			if(reloadTimer > 0.0f) {
				reloadTimer -= Time.deltaTime;
			}
			if(reloadTimer <= 0.0f) {
				reloading = false;
				ammoSource.ammoCur += magazineCur;
				magazineCur = Mathf.Min(magazineMaximum,ammoSource.ammoCur);
				ammoSource.ammoCur -= magazineCur;
				SendMessage("OnReloadStop",this,SendMessageOptions.DontRequireReceiver);
				SendMessage("OnReloadComplete",this,SendMessageOptions.DontRequireReceiver);
			}
		} else {
			if(weapon.triggerDown && magazineCur == 0 && automaticReload) {
				reloadBegin();
			}
		}
	}

	private void aquireAmmoSource() {
		if(weapon.wielder != null) {
			var sources = weapon.wielder.transform.GetComponents<CharacterAmmo>();
			foreach(var source in sources) {
				if(source.ammoType == ammoType) {
					ammoSource = source;
					break;
				}
			}
		}
	}

	private void OnReloadBegin() {
		if(weapon.handAnimatior != null) {
			weapon.handAnimatior.SetTrigger("OnReload");
			weapon.handAnimatior.speed = 1.0f / reloadTime;
		}
	}

	private void OnReloadStop() {
		if(weapon.handAnimatior != null) {
			//weapon.handAnimatior.ResetTrigger("OnReload");
			weapon.handAnimatior.speed = 1.0f;
		}
	}

	private void OnWeaponEquip() {
		if(rigidbody != null) {
			rigidbody.detectCollisions = false;
			rigidbody.isKinematic = true;
		}
	}

	private void OnWeaponUnequip() {
		if(rigidbody != null) {
			rigidbody.detectCollisions = true;
			rigidbody.isKinematic = false;
		}
	}
}