﻿using UnityEngine;
using System.Collections;

public class BossMove : MonoBehaviour {

    private Transform CurrTarget;
    public Transform NextTarget;
    public int TargetId = 0;
    public float Distance;
    public float Speed;
    private float StartTime;
    private float Duration = 1.0f;

	// Use this for initialization
	void Start () 
    {
        StartTime = Time.time;
        Speed = 1.0f;

        CurrTarget = GameObject.Find("Waypoint0").transform;
        NextTarget = GameObject.Find("Waypoint1").transform;

        transform.position = CurrTarget.position;
	}
	
	// Update is called once per frame
	void Update () 
    {
        Distance = Vector3.Distance(NextTarget.position, transform.position);
       
        if (Distance < 5)
        {
            TargetId = Random.Range(0, 5);
            CurrTarget = NextTarget;
            NextTarget = GameObject.Find("Waypoint" + TargetId).transform;
        }

        transform.position = Vector3.Lerp(transform.position, NextTarget.position, Time.deltaTime * Speed);
	}
}
