﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class HTCrazyBumpTextureSettings {
	[Tooltip("Detail texture: Displacement(R), OcclusionMap(G), Normal Map(BA) (Z is calculated at runtime)")]
	public Texture2D detailMap;

	[Range(-2,2)]
	public float dispMax = 0.3f;

	[Range(0.001f,2)]
	public float occPower = 1.0f;

	[Range(0,1)]
	public float specShininess = 0.75f;

	[Range(0,1)]
	public float specPower = 0.25f;
}
