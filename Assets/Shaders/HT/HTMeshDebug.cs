﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class HTMeshDebug : MonoBehaviour {
	void OnDrawGizmos() {
		MeshFilter meshFilter = GetComponent<MeshFilter>();

		if(meshFilter != null) {
			for(int i = 0; i < meshFilter.sharedMesh.vertexCount; i++) {
				Vector3 point = transform.TransformPoint(meshFilter.sharedMesh.vertices[i]);
				Vector3 norm = transform.TransformDirection(meshFilter.sharedMesh.normals[i]);
				Vector3 colnorm = transform.TransformDirection(new Vector3(meshFilter.sharedMesh.colors[i][0] * 2 - 1,meshFilter.sharedMesh.colors[i][1] * 2 - 1,meshFilter.sharedMesh.colors[i][2] * 2 - 1));

				Gizmos.color = Color.red;
				Gizmos.DrawRay(point,norm);

				Gizmos.color = Color.green;
				Gizmos.DrawRay(point,colnorm);
			}
		}
	}
}
