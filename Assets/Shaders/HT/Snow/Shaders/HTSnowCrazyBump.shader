﻿Shader "HT/Snow/HTSnowCrazyBump" {
	Properties {
		_MainTex ("Base(RGB) Specularity(A)", 2D) = "white" {}
		_DetailTex ("Displacement(R) Occlusion(G) Normal(BA)", 2D) = "white" {}
		_DetailDispMax ("Displacement Max", Range (0, 1)) = 0.3
		_DetailOccPower ("Occlusion Power", Range (0.001, 2)) = 1.0
		_DetailSpecShininess ("Specular Shininess", Range (0, 1)) = 0.75
		_DetailSpecPower ("Specular Power", Range (0, 1)) = 0.25
		_SnowStrength ("Snow Strength", Range (0, 1)) = 1
	}
	
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf BlinnPhong addshadow fullforwardshadows vertex:vert tessellate:tessEdge nolightmap
		#include "UnityCG.cginc"
		#include "Tessellation.cginc"
		#pragma target 5.0
		
		#include "HTSnowCrazyBumpInc.cginc"

		sampler2D _MainTex,_DetailTex;
		float _DetailDispMax,_DetailOccPower,_DetailSpecShininess,_DetailSpecPower,_SnowStrength;
		
		uniform float4 _DetailTex_ST;

		struct Input {
			float3 worldPos;
			float2 uv_MainTex : TEXCOORD0;
			float4 color : COLOR;
		};
		
		struct appdata {
			float4 vertex : POSITION;
			float3 normal : NORMAL;
			float4 tangent : TANGENT;
			float2 texcoord : TEXCOORD0;
			float4 color : COLOR;
		};
		
		float4 tessEdge(appdata v0, appdata v1, appdata v2) {
			return UnityEdgeLengthBasedTessCull(v0.vertex, v1.vertex, v2.vertex, _HTSnow_TesselationEdgeLength,_HTSnow_TesselationFrustumCullDistance);
		}
		
		void vert(inout appdata v) {
			// must have tangent for bump mapping
			v.tangent.xyz = cross(v.normal, float3(0,0,1));
			v.tangent.w = -1;
			
			float dispTex = tex2Dlod(_DetailTex,float4(TRANSFORM_TEX(v.texcoord,_DetailTex).xy,0,0)).r * _DetailDispMax;
			
			float3 worldNormal = mul(_Object2World,float4(v.normal,0.0)).xyz;
			
			// snow
			v.color.a = _SnowStrength * HTSnowCalculateSnowVertexPower(normalize(mul(_Object2World,v.normal)),mul(_Object2World,v.vertex).y);
			float dispSnow = HTSnowCalculateSnowVertexDisplacement(v.texcoord);
			
			v.vertex.xyz += (v.normal * ((1-pow(v.color.a,0.5))*dispTex + pow(v.color.a,1.5)*dispSnow));
		}
		
		void surf(Input IN, inout SurfaceOutput o) {
			_SpecColor = float4(1,1,1,1);
			
			float4 diff = tex2D(_MainTex, IN.uv_MainTex);
			float4 detail = tex2D(_DetailTex, IN.uv_MainTex);
			
			o.Albedo.rgb = diff.rgb * pow(detail.g,_DetailOccPower);
			o.Normal.xy = detail.ba * 2 - 1;
			o.Normal.z = sqrt(abs(1 - o.Normal.x*o.Normal.x - o.Normal.y*o.Normal.y));
			
			o.Specular = _DetailSpecShininess;
			o.Gloss = diff.a * _DetailSpecPower;
			
			HTSnowPixelApplySnow(o.Albedo.rgb,o.Normal.xyz,o.Specular,o.Gloss,IN.uv_MainTex,IN.color.a,3,4,3);
			
			o.Alpha = 0;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
