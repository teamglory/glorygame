#ifndef HT_CG_CRAZYBUMPSNOW
#define HT_CG_CRAZYBUMPSNOW

// global variables needed by all HT CrazyBump Snow Shaders

float _HTSnow_TesselationEdgeLength;
float _HTSnow_TesselationFrustumCullDistance;

sampler2D _HTSnow_Tex;
sampler2D _HTSnow_TexDetail;
float _HTSnow_TexDetail_DispMax;
float _HTSnow_TexDetail_OccPower;
float _HTSnow_TexDetail_SpecShininess;
float _HTSnow_TexDetail_SpecPower;
float _HTSnow_TexTileX;
float _HTSnow_TexTileY;
float _HTSnow_TexTileOffsetX;
float _HTSnow_TexTileOffsetY;
float4 _HTSnow_Direction;
float _HTSnow_Depth;
float _HTSnow_StartHeight;
float _HTSnow_RampUpDistance;

inline float HTSnowCalculateSnowVertexPower(in float3 vertexNormal,in float vertexY) {
	float h = dot(vertexNormal,-_HTSnow_Direction.xyz);
	if(h > 0) return pow(h,0.5) * clamp((vertexY - _HTSnow_StartHeight)*(1/_HTSnow_RampUpDistance),0,1);
	return 0;
}

inline float HTSnowCalculateSnowVertexDisplacement(in float2 snowTexCoord) {
	return (_HTSnow_Depth + tex2Dlod(_HTSnow_TexDetail,float4(snowTexCoord.xy,0,0)).r * _HTSnow_TexDetail_DispMax);
}

// reccommended extra parameters: h=3, j=4, k=3
// +h = -snow area - snow fade-in distance
// +k = +snow area - snow fade in distance
//inline float HTSnowCalculateSnowPixelSnowWeight(in float snowPower,in float snowDisplacement,in float h,in float j,in float k) {
//	return clamp(pow(snowPower*(lerp(snowDisplacement, 1, snowPower)), h) * j * (1 + k * _HTSnow_Depth),  0,1);
//}

inline float3 HTMatrixScale(in float4x4 mat) {
	return float3(
	length(float3(mat[0][0], mat[1][0], mat[2][0])),
	length(float3(mat[0][1], mat[1][1], mat[2][1])),
	length(float3(mat[0][2], mat[1][2], mat[2][2])));
}

inline void HTSnowPixelApplySnow(inout half3 color,inout half3 normal,inout half specular,inout half gloss,in float2 snowTexCoord,in float snowPower,in float h,in float j,in float k) {
	if(snowPower > 0) {
		fixed4 snowDiff = tex2D(_HTSnow_Tex,snowTexCoord);
		fixed4 snowDetail = tex2D(_HTSnow_TexDetail,snowTexCoord);
		
		fixed3 snowNormal;
		snowNormal.xy = snowDetail.ba * 2 - 1;
		snowNormal.z = sqrt(abs(1 - snowNormal.x*snowNormal.x - snowNormal.y*snowNormal.y));
		
		fixed3 snowCol = snowDiff.rgb * pow(abs(snowDetail.g),_HTSnow_TexDetail_OccPower);
		float snowSpec = _HTSnow_TexDetail_SpecShininess;
		float snowGloss = snowDiff.a * _HTSnow_TexDetail_SpecPower;
		
		float groundWeight = 1 - clamp(pow(snowPower*(lerp(snowDetail.r, 1, snowPower)), h) * j * (1 + k * _HTSnow_Depth),  0,1);//HTSnowCalculateSnowPixelSnowWeight(snowPower,snowDisplacement,h,j,k);
		
		color = (groundWeight*color + (1 - groundWeight)*snowCol);
		normal = (groundWeight*normal + (1 - groundWeight)*snowNormal);
		specular = (groundWeight*specular + (1 - groundWeight)*snowSpec);
		gloss = (groundWeight*gloss + (1 - groundWeight)*snowGloss);
	}
}

#endif