﻿Shader "HT/Snow/HTSnowCrazyBumpNoDisplacement" {
	Properties {
		_MainTex ("Base(RGB) Specularity(A)", 2D) = "white" {}
		_DetailTex ("Displacement(R) Occlusion(G) Normal(BA)", 2D) = "white" {}
		_DetailOccPower ("Occlusion Power", Range (0.001, 2)) = 1.0
		_DetailSpecShininess ("Specular Shininess", Range (0, 1)) = 0.75
		_DetailSpecPower ("Specular Power", Range (0, 1)) = 0.25
		_SnowStrength ("Snow Strength", Range (0, 1)) = 1
	}
	
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf BlinnPhong addshadow fullforwardshadows vertex:vert nolightmap
		#include "UnityCG.cginc"
		#pragma target 5.0
		
		#include "HTSnowCrazyBumpInc.cginc"

		sampler2D _MainTex,_DetailTex;
		float _DetailDispMax,_DetailOccPower,_DetailSpecShininess,_DetailSpecPower,_SnowStrength;
		
		uniform float4 _DetailTex_ST;

		struct Input {
			float3 worldPos;
			float2 uv_MainTex : TEXCOORD0;
			float4 color : COLOR;
		};
		
		struct appdata {
			float4 vertex : POSITION;
			float3 normal : NORMAL;
			float4 tangent : TANGENT;
			float2 texcoord : TEXCOORD0;
			float4 color : COLOR;
		};
		
		void vert(inout appdata v) {
			// hold on to world normal
			v.color.rgb = v.normal;
			
			// must have tangent for bump mapping
			v.tangent.xyz = cross(v.normal, float3(0,0,1));
			v.tangent.w = -1;
			
			// snow
			v.color.a = _SnowStrength * HTSnowCalculateSnowVertexPower(normalize(mul(_Object2World,v.normal)),mul(_Object2World,v.vertex).y);
		}
		
		void surf(Input IN, inout SurfaceOutput o) {
			_SpecColor = float4(1,1,1,1);
			
			float4 diff = tex2D(_MainTex, IN.uv_MainTex);
			float4 detail = tex2D(_DetailTex, IN.uv_MainTex);
			
			o.Albedo.rgb = diff.rgb * pow(detail.g,_DetailOccPower);
			o.Normal.xy = detail.ba * 2 - 1;
			o.Normal.z = sqrt(abs(1 - o.Normal.x*o.Normal.x - o.Normal.y*o.Normal.y));
			
			o.Specular = _DetailSpecShininess;
			o.Gloss = diff.a * _DetailSpecPower;
			
			//float2 snowUV;
			//snowUV.x = IN.worldPos.x / _HTSnow_TexTileX + _HTSnow_TexTileOffsetX;
			//snowUV.x = IN.worldPos.y / _HTSnow_TexTileY + _HTSnow_TexTileOffsetY;
			
			HTSnowPixelApplySnow(o.Albedo.rgb,o.Normal.xyz,o.Specular,o.Gloss,IN.uv_MainTex,IN.color.a,3,4,3);
			
			o.Alpha = 0;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
