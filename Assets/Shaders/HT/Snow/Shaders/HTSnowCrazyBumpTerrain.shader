﻿Shader "HT/Snow/HTSnowCrazyBumpTerrain" {
	Properties {
		[HideInInspector] _Control ("Control (RGBA)", 2D) = "red" {}
		[HideInInspector] _Splat3 ("Layer 3 (A)", 2D) = "white" {}
		[HideInInspector] _Splat2 ("Layer 2 (B)", 2D) = "white" {}
		[HideInInspector] _Splat1 ("Layer 1 (G)", 2D) = "white" {}
		[HideInInspector] _Splat0 ("Layer 0 (R)", 2D) = "white" {}
		// used in fallback on old cards & base map
		[HideInInspector] _MainTex ("BaseMap (RGB)", 2D) = "white" {}
		[HideInInspector] _Color ("Main Color", Color) = (1,1,1,1)
		
		_Detail0 ("Detail 0: Displacement (R), Occlusion(G), Normal(BA)", 2D) = "white" {}
		_DispMax0 ("Displacement Max 0", Range (0, 1)) = 0.3
		_OccPower0 ("Occlusion Power 0", Range (0.001, 2)) = 1.0
		_SpecShininess0 ("Specular Shininess 0", Range (0, 1)) = 0.75
		_SpecPower0 ("Specular Power 0", Range (0, 1)) = 0.25
		_HTSnow_SnowStrengthTex0 ("Snow Strength 0", Range (0, 1)) = 1
		
		_Detail1 ("Detail 1: Displacement (R), Occlusion(G), Normal(BA)", 2D) = "white" {}
		_DispMax1 ("Displacement Max 1", Range (0, 1)) = 0.3
		_OccPower1 ("Occlusion Power 1", Range (0.001, 2)) = 1.0
		_SpecShininess1 ("Specular Shininess 1", Range (0, 1)) = 0.75
		_SpecPower1 ("Specular Power 1", Range (0, 1)) = 0.25
		_HTSnow_SnowStrengthTex1 ("Snow Strength 1", Range (0, 1)) = 1
		
		_Detail2 ("Detail 2: Displacement (R), Occlusion(G), Normal(BA)", 2D) = "white" {}
		_DispMax2 ("Displacement Max 2", Range (0, 1)) = 0.3
		_OccPower2 ("Occlusion Power 2", Range (0.001, 2)) = 1.0
		_SpecShininess2 ("Specular Shininess 2", Range (0, 1)) = 0.75
		_SpecPower2 ("Specular Power 2", Range (0, 1)) = 0.25
		_HTSnow_SnowStrengthTex2 ("Snow Strength 2", Range (0, 1)) = 1
		
		_Detail3 ("Detail 3: Displacement (R), Occlusion(G), Normal(BA)", 2D) = "white" {}
		_DispMax3 ("Displacement Max 3", Range (0, 1)) = 0.3
		_OccPower3 ("Occlusion Power 3", Range (0.001, 2)) = 1.0
		_SpecShininess3 ("Specular Shininess 3", Range (0, 1)) = 0.75
		_SpecPower3 ("Specular Power 3", Range (0, 1)) = 0.25
		_HTSnow_SnowStrengthTex3 ("Snow Strength 3", Range (0, 1)) = 1
		
		_HTTerrain_HeightOffset ("Terrain Hieght Offset", float) = 15.0
		
		_TerrainSizeX ("Terrain Size X", float) = 1000.0
		_TerrainSizeY ("Terrain Size Y", float) = 1000.0
	}
		
	SubShader {
		Tags {
			"SplatCount" = "4"
			"Queue" = "Geometry-100"
			"RenderType" = "Opaque"
		}
		
		LOD 300
		
		CGPROGRAM
		#pragma surface surf BlinnPhong addshadow fullforwardshadows vertex:vert tessellate:tessEdge nolightmap
		#include "UnityCG.cginc"
		#include "Tessellation.cginc"
		#pragma target 5.0
		
		#include "HTSnowCrazyBumpInc.cginc"
		
		sampler2D _Control;
		sampler2D _Splat0,_Splat1,_Splat2,_Splat3;
		sampler2D _Detail0,_Detail1,_Detail2,_Detail3;
		float _DispMax0,_DispMax1,_DispMax2,_DispMax3;
		float _OccPower0,_OccPower1,_OccPower2,_OccPower3;
		float _SpecShininess0,_SpecShininess1,_SpecShininess2,_SpecShininess3;
		float _SpecPower0,_SpecPower1,_SpecPower2,_SpecPower3;
		float _TileX0,_TileX1,_TileX2,_TileX3;
		float _TileY0,_TileY1,_TileY2,_TileY3;
		float _TileOffsetX0,_TileOffsetX1,_TileOffsetX2,_TileOffsetX3;
		float _TileOffsetY0,_TileOffsetY1,_TileOffsetY2,_TileOffsetY3;
		float _TerrainSizeX,_TerrainSizeZ;
		
		float _HTTerrain_HeightOffset;
		
		// snow-specific
		float _HTSnow_SnowStrengthTex0,_HTSnow_SnowStrengthTex1,_HTSnow_SnowStrengthTex2,_HTSnow_SnowStrengthTex3;
		
		inline float2 HTSnowCrazyBumpTerrainCalculateSnowUV(in float2 controlUV) {
			return float2(controlUV.x * (_TerrainSizeX/_HTSnow_TexTileX+_HTSnow_TexTileOffsetX),controlUV.y * (_TerrainSizeZ/_HTSnow_TexTileY+_HTSnow_TexTileOffsetY));
		}
		
		struct Input {
			float3 worldPos;
			float2 uv_Control : TEXCOORD0;
			float2 uv_Splat0 : TEXCOORD1;
			float2 uv_Splat1 : TEXCOORD2;
			float2 uv_Splat2 : TEXCOORD3;
			float2 uv_Splat3 : TEXCOORD4;
			float4 color : COLOR;
		};
		
		struct appdata {
			float4 vertex : POSITION;
			float4 tangent : TANGENT;
			float3 normal : NORMAL;
			float2 texcoord : TEXCOORD0;
			float4 color : COLOR;
		};
		
		float4 tessEdge(appdata v0, appdata v1, appdata v2) {
			return UnityEdgeLengthBasedTessCull(v0.vertex, v1.vertex, v2.vertex, _HTSnow_TesselationEdgeLength,_HTSnow_TesselationFrustumCullDistance);
		}
		
		void vert(inout appdata v) {
			// must have tangent for bump mapping
			v.tangent.xyz = cross(v.normal, float3(0,0,1));
			v.tangent.w = -1;
			
			// calculate and apply displacement
			// must calculate the uv coords for all displacement maps
			fixed4 splat_control = tex2Dlod(_Control,float4(v.texcoord.xy,0,0));
			float dispSplats;
			float snowStrength;
			
			// NOTE: for some reason the x component of the UV is correct, while the y component is double what it should be...
			dispSplats = splat_control.r * tex2Dlod(_Detail0,float4(v.texcoord.x * (_TerrainSizeX/_TileX0+_TileOffsetX0),v.texcoord.y * (_TerrainSizeZ/_TileY0+_TileOffsetY0),0,0)).r * _DispMax0;
			snowStrength = splat_control.r * _HTSnow_SnowStrengthTex0;
			dispSplats += splat_control.g * tex2Dlod(_Detail1,float4(v.texcoord.x * (_TerrainSizeX/_TileX1+_TileOffsetX1),v.texcoord.y * (_TerrainSizeZ/_TileY1+_TileOffsetY1),0,0)).r * _DispMax1;
			snowStrength += splat_control.g * _HTSnow_SnowStrengthTex1;
			dispSplats += splat_control.b * tex2Dlod(_Detail2,float4(v.texcoord.x * (_TerrainSizeX/_TileX2+_TileOffsetX2),v.texcoord.y * (_TerrainSizeZ/_TileY2+_TileOffsetY2),0,0)).r * _DispMax2;
			snowStrength += splat_control.b * _HTSnow_SnowStrengthTex2;
			dispSplats += splat_control.a * tex2Dlod(_Detail3,float4(v.texcoord.x * (_TerrainSizeX/_TileX3+_TileOffsetX3),v.texcoord.y * (_TerrainSizeZ/_TileY3+_TileOffsetY3),0,0)).r * _DispMax3;
			snowStrength += splat_control.a * _HTSnow_SnowStrengthTex3;
			
			// snow
			// already in world space, otherwise would have to call: mul(_Object2World,v.vertex)
			v.color.a = snowStrength * HTSnowCalculateSnowVertexPower(v.normal,v.vertex.y + _HTTerrain_HeightOffset);
			float dispSnow = HTSnowCalculateSnowVertexDisplacement(HTSnowCrazyBumpTerrainCalculateSnowUV(v.texcoord));
			
			v.vertex.xyz += (v.normal * ((1-pow(v.color.a,0.5))*dispSplats + pow(v.color.a,1.5)*dispSnow));
			
			v.vertex.y += _HTTerrain_HeightOffset;
		}
		
		void surf(Input IN, inout SurfaceOutput o) {
			fixed4 splat_control = tex2D(_Control, IN.uv_Control);
			
			float2 snowUV = HTSnowCrazyBumpTerrainCalculateSnowUV(IN.uv_Control);
			
			fixed4 detail0 = tex2D(_Detail0, IN.uv_Splat0);
			fixed4 detail1 = tex2D(_Detail1, IN.uv_Splat1);
			fixed4 detail2 = tex2D(_Detail2, IN.uv_Splat2);
			fixed4 detail3 = tex2D(_Detail3, IN.uv_Splat3);
			
			fixed4 splat0 = tex2D(_Splat0, IN.uv_Splat0);
			fixed4 splat1 = tex2D(_Splat1, IN.uv_Splat1);
			fixed4 splat2 = tex2D(_Splat2, IN.uv_Splat2);
			fixed4 splat3 = tex2D(_Splat3, IN.uv_Splat3);
			
			// unpack normals from BA in the detail texture
			// NOTE, sometimes the normals can contain a 1 for the x or y component
			// when that occurs sqrt will return INF, which is bad, so bias the calculation slightly to avoid this
			half3 normal0,normal1,normal2,normal3;
			normal0.xy = detail0.ba * 2 - 1;
			normal0.z = sqrt(abs(1 - normal0.x*normal0.x - normal0.y*normal0.y));
			normal1.xy = detail1.ba * 2 - 1;
			normal1.z = sqrt(abs(1 - normal1.x*normal1.x - normal1.y*normal1.y));
			normal2.xy = detail2.ba * 2 - 1;
			normal2.z = sqrt(abs(1 - normal2.x*normal2.x - normal2.y*normal2.y));
			normal3.xy = detail3.ba * 2 - 1;
			normal3.z = sqrt(abs(1 - normal3.x*normal3.x - normal3.y*normal3.y));
			
			half3 col;
			col = splat_control.r * splat0.rgb * pow(detail0.g,_OccPower0);
			col += splat_control.g * splat1.rgb * pow(detail1.g,_OccPower1);
			col += splat_control.b * splat2.rgb * pow(detail2.g,_OccPower2);
			col += splat_control.a * splat3.rgb * pow(detail3.g,_OccPower3);
		
			half3 nrm;
			nrm = splat_control.r * normal0;
			nrm += splat_control.g * normal1;
			nrm += splat_control.b * normal2;
			nrm += splat_control.a * normal3;
			
			half spec,gloss;	
			spec = splat_control.r * _SpecShininess0;
			gloss = splat_control.r * splat0.a * _SpecPower0;
			spec += splat_control.g * _SpecShininess1;
			gloss += splat_control.g * splat1.a * _SpecPower1;
			spec += splat_control.b * _SpecShininess2;
			gloss += splat_control.b * splat2.a * _SpecPower2;
			spec += splat_control.a * _SpecShininess3;
			gloss += splat_control.a * splat3.a * _SpecPower3;
			
			_SpecColor = float4(1,1,1,1);
			
			HTSnowPixelApplySnow(col,nrm,spec,gloss,snowUV,IN.color.a,3,4,3);
			
			o.Albedo = col;
			o.Alpha = 0.0;
			o.Gloss = gloss;
			o.Specular = spec;
			o.Normal = nrm;
			//o.Albedo = nrm * 0.5 + 0.5; // visualize normals
		}
		ENDCG
	}
	
	// Fallback to Diffuse
	Fallback "Diffuse"
}
