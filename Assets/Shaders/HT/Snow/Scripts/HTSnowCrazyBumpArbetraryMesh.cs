﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class HTSnowCrazyBumpArbetraryMesh : MonoBehaviour {

	// calculates special normals for the sharedMesh that will be used for
	// displacing said verts in the shader, these new normals will be
	// stored in the color value of each vert
	private void Update() {
		const float _TOL_TOUCHING = 0.001f;

		MeshFilter meshFilter = GetComponent<MeshFilter>();

		if(meshFilter != null) {
			var listVertNorms = new List<Vector3>(meshFilter.sharedMesh.vertexCount);

			// populate the initiial list with the coorosponding normals
			for(int i = 0; i < meshFilter.sharedMesh.vertexCount; i++) {
				listVertNorms.Add(meshFilter.sharedMesh.normals[i]);
			}

			for(int i = 0; i < meshFilter.sharedMesh.vertexCount; i++) {
				for(int j = i; j < meshFilter.sharedMesh.vertexCount; j++) {
					if(i == j) continue;

					if(Vector3.Distance(meshFilter.sharedMesh.vertices[i],meshFilter.sharedMesh.vertices[j]) <= _TOL_TOUCHING) {
						if(listVertNorms[j].sqrMagnitude > 0) {
							listVertNorms[i] = listVertNorms[j] = (listVertNorms[i] + listVertNorms[j]) / 2.0f;
						}
					}
				}
			}

			Color[] newcolors = new Color[meshFilter.sharedMesh.vertexCount];
			for(int i = 0; i < meshFilter.sharedMesh.vertexCount; i++) {
				newcolors[i].r = listVertNorms[i].x * 0.5f + 0.5f;
				newcolors[i].g = listVertNorms[i].y * 0.5f + 0.5f;
				newcolors[i].b = listVertNorms[i].z * 0.5f + 0.5f;
			}
			meshFilter.sharedMesh.colors = newcolors;
		}
	}
}