﻿using System;
using UnityEngine;

// Original Code provided by Chris Morris of Six Times Nothing (http://www.sixtimesnothing.com)
// Modified to support snow by lars bertram (http://forum.unity3d.com/members/3642-larsbertram1)

// Modified again to support DX11 Tessellation and displacement maps by Jacob Tyndall (Hacktank@Hacktank.NET)

[ExecuteInEditMode]
[RequireComponent(typeof(Terrain))]
public class HTSnowCrazyBumpTerrain : MonoBehaviour {

	[System.Serializable]
	public class HTCrazyBumpSplatTextureSettings : HTCrazyBumpTextureSettings {
		[Range(0,1)]
		public float snowStrength = 1.0f;
	}

	public HTCrazyBumpSplatTextureSettings splat0;
	public HTCrazyBumpSplatTextureSettings splat1;
	public HTCrazyBumpSplatTextureSettings splat2;
	public HTCrazyBumpSplatTextureSettings splat3;

	public float graphicsOffsetY = -0.2f;

	private Material tempmaterial = null;

	private void Start() {
		SyncShaderData();
	}

	private void Update() {
		if(tempmaterial == null) {
			tempmaterial = new Material(Shader.Find("HT/Snow/HTSnowCrazyBumpTerrain"));
			Terrain terrainComp = (Terrain)GetComponent(typeof(Terrain));
			terrainComp.materialTemplate = tempmaterial;
		}

		SyncShaderData();
	}

	private void SyncShaderData() {
		Terrain terrainComp = (Terrain)GetComponent(typeof(Terrain));

		for(int i = 0; i < 5; i++) {
			//base HTCrazyBumpTextureSettings info
			HTCrazyBumpTextureSettings target;
			switch(i) {
				case 0:
					target = splat0;
					break;
				case 1:
					target = splat1;
					break;
				case 2:
					target = splat2;
					break;
				case 3:
					target = splat3;
					break;
				default:
					target = null;
					break;
			}
			if(target != null) {
				if(target.detailMap != null) {
					terrainComp.materialTemplate.SetTexture("_Detail" + i,target.detailMap);
					terrainComp.materialTemplate.SetFloat("_DispMax" + i,target.dispMax);
					terrainComp.materialTemplate.SetFloat("_OccPower" + i,target.occPower);
					terrainComp.materialTemplate.SetFloat("_SpecShininess" + i,target.specShininess);
					terrainComp.materialTemplate.SetFloat("_SpecPower" + i,target.specPower);
				} else {
					terrainComp.materialTemplate.SetTexture("_Detail" + i,null);
					terrainComp.materialTemplate.SetFloat("_DispMax" + i,0);
					terrainComp.materialTemplate.SetFloat("_OccPower" + i,0);
					terrainComp.materialTemplate.SetFloat("_SpecShininess" + i,target.specShininess);
					terrainComp.materialTemplate.SetFloat("_SpecPower" + i,0);
				}

				if(i <= 3) {
					if(terrainComp.terrainData.splatPrototypes.Length >= i+1 && terrainComp.terrainData.splatPrototypes[i].texture != null) {
						terrainComp.materialTemplate.SetFloat("_HTSnow_SnowStrengthTex" + i,(target as HTCrazyBumpSplatTextureSettings).snowStrength);

						terrainComp.materialTemplate.SetFloat("_TileX" + i,terrainComp.terrainData.splatPrototypes[i].tileSize.x);
						terrainComp.materialTemplate.SetFloat("_TileY" + i,terrainComp.terrainData.splatPrototypes[i].tileSize.y);
						terrainComp.materialTemplate.SetFloat("_TileOffsetX" + i,terrainComp.terrainData.splatPrototypes[i].tileOffset.x);
						terrainComp.materialTemplate.SetFloat("_TileOffsetY" + i,terrainComp.terrainData.splatPrototypes[i].tileOffset.y);
					} else {
						terrainComp.materialTemplate.SetFloat("_HTSnow_SnowStrengthTex" + i,0);
					}
				}
			}
		}

		terrainComp.materialTemplate.SetFloat("_TerrainSizeX",terrainComp.terrainData.size.x);
		terrainComp.materialTemplate.SetFloat("_TerrainSizeZ",terrainComp.terrainData.size.z);

		terrainComp.materialTemplate.SetFloat("_HTTerrain_HeightOffset",graphicsOffsetY);
	}
}