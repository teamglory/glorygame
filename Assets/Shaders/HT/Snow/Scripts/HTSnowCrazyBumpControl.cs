﻿using UnityEngine;

[ExecuteInEditMode]
public class HTSnowCrazyBumpControl : MonoBehaviour {

	[System.Serializable]
	public class HTCrazyBumpSnowTextureSettings : HTCrazyBumpTextureSettings {

		[Tooltip("Diffuse+Spec: Displacement(RGB), Specularity(A)")]
		public Texture2D diffuseSpecMap;

		public Vector2 tileSize = new Vector2(15,15);
		public Vector2 tileOffset = Vector2.zero;

		public Vector3 direction = Vector3.down;

		[Range(0,5)]
		public float depth = 0.25f;

		public float startHeight = 30.0f;
		public float rampUpDistance = 10.0f;
	}

	public HTCrazyBumpSnowTextureSettings snow = new HTCrazyBumpSnowTextureSettings();

	[Range(2,100)]
	public float tessEdgeLength = 15.0f;

	[Range(5,100)]
	public float tessCullDistance = 20.0f;

	private void Start() {
		SyncShaderData();
	}

	private void Update() {
		SyncShaderData();
	}

	private void SyncShaderData() {
		if(snow.detailMap != null) {
			Shader.SetGlobalTexture("_HTSnow_TexDetail",snow.detailMap);
			Shader.SetGlobalFloat("_HTSnow_TexDetail_DispMax",snow.dispMax);
			Shader.SetGlobalFloat("_HTSnow_TexDetail_OccPower",snow.occPower);
			Shader.SetGlobalFloat("_HTSnow_TexDetail_SpecShininess",snow.specShininess);
			Shader.SetGlobalFloat("_HTSnow_TexDetail_SpecPower",snow.specPower);
		} else {
			Shader.SetGlobalTexture("_HTSnow_TexDetail",null);
			Shader.SetGlobalFloat("_HTSnow_TexDetail_DispMax",0);
			Shader.SetGlobalFloat("_HTSnow_TexDetail_OccPower",0);
			Shader.SetGlobalFloat("_HTSnow_TexDetail_SpecShininess",snow.specShininess);
			Shader.SetGlobalFloat("_HTSnow_TexDetail_SpecPower",0);
		}
		if(snow.diffuseSpecMap != null) {
			Shader.SetGlobalTexture("_HTSnow_Tex",snow.diffuseSpecMap);
			Shader.SetGlobalFloat("_HTSnow_TexTileX",snow.tileSize.x);
			Shader.SetGlobalFloat("_HTSnow_TexTileY",snow.tileSize.y);
			Shader.SetGlobalFloat("_HTSnow_TexTileOffsetX",snow.tileOffset.x);
			Shader.SetGlobalFloat("_HTSnow_TexTileOffsetY",snow.tileOffset.y);

			Shader.SetGlobalVector("_HTSnow_Direction",snow.direction.normalized);
			Shader.SetGlobalFloat("_HTSnow_Depth",snow.depth);
			Shader.SetGlobalFloat("_HTSnow_StartHeight",snow.startHeight);
			Shader.SetGlobalFloat("_HTSnow_RampUpDistance",snow.rampUpDistance);
		} else {
			Shader.SetGlobalTexture("_HTSnow_Tex",null);
			Shader.SetGlobalVector("_HTSnow_Direction",Vector3.zero);
			Shader.SetGlobalFloat("_HTSnow_Depth",0);
			Shader.SetGlobalFloat("_HTSnow_StartHeight",99999);
			Shader.SetGlobalFloat("_HTSnow_RampUpDistance",0);
		}

		Shader.SetGlobalFloat("_HTSnow_TesselationEdgeLength",tessEdgeLength);
		Shader.SetGlobalFloat("_HTSnow_TesselationFrustumCullDistance",tessCullDistance);
	}
}